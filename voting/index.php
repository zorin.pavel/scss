<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Голосование");
?>

<?
$arGroupAvalaible = array("9"); // массив групп, которые в которых нужно проверить доступность пользователя
$arGroups = CUser::GetUserGroup($USER->GetID()); // массив групп, в которых состоит пользователь
$result_intersect = array_intersect($arGroupAvalaible, $arGroups);// далее проверяем, если пользователь вошёл хотя бы в одну из групп, то позволяем ему что-либо делать
?>

<div class="banner-container">
	<div class="banner">
		<h1 class="banner-title">Голосование 2024</h1>
		<p class="banner-text">Поддержи историю, которая тебя вдохновила</p>
	</div>
</div>

<div class="running-textstring-container">
	<div class="running-textstring">
		<?
			for($i = 0; $i <= 8; $i++) {
				?>
					<span class="running-textstring-text">
						 Голосование с 3 июня
					</span>
				<?
			}
		?>
	</div>
</div>

<div class="voting-miniapp-container">
	<div class="voting-miniapp">
		<div class="voting-miniapp-icon-container">
			<img width="48" src="/upload/medialibrary/be4/h9v8v5ws69fxzpbvuo4k1tpjfksckuem.png" height="49">
		</div>
		<div class="voting-miniapp-content">
			<p class="voting-miniapp-text">Голосование проходит в приложении VK Mini Apps. Просто и удобно.</p>
			<p class="voting-miniapp-text">Чтобы оценить номинанта, нужно иметь аккаунт ВКонтакте. Из карточки участника перейдешь по ссылке в приложение для голосования. За каждого участника можно отдать только 1 голос — всего их 3. Воспользуйся ими, чтобы поддержать других талантливых героев.</p>
		</div>
	</div>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.filter",
	"template1",
	Array(
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"FIELD_CODE" => array(0=>"NAME",1=>"",),
		"FILTER_NAME" => "arrFilter",
		"IBLOCK_ID" => "",
		"IBLOCK_TYPE" => "",
		"LIST_HEIGHT" => "5",
		"NUMBER_WIDTH" => "5",
		"PAGER_PARAMS_NAME" => "arrPager",
		"PREFILTER_NAME" => "preFilter",
		"PRICE_CODE" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"SAVE_IN_SESSION" => "N",
		"TEXT_WIDTH" => "20",
		"PAGE_ADDRESS" => "/voting",
		"ONLY_ON_VOTING" => "Y"
	)
);?>&nbsp;
<?
GLOBAL $arrFilter;
if (!is_array($arrFilter))
   $arrFilter = array();

$categoryNew = $_GET['category'];
if ($categoryNew == 'tolk') {
	$arrFilter['PROPERTY_CATEGORY_NEW'] = 42;
}
if ($categoryNew == 'top-children') {
	$arrFilter['PROPERTY_CATEGORY_NEW'] = 43;
}
if ($categoryNew == 'all') {
	unset($arrFilter['PROPERTY_CATEGORY_NEW']);
}
$arrFilter['PROPERTY_STATUS'] = 46;
?>

	<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"pretendenty",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "pretendenty",
		"DETAIL_URL" => "/voting/info/?ELEMENT_ID=#ELEMENT_ID#",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"ID",1=>"CODE",2=>"XML_ID"),
		"FILTER_NAME" => "arrFilter",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "persons",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "500",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "modern",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "7",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"CATEGORY_NEW"),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "UPDATED",
		"SORT_BY2" => "",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "",
		"STRICT_SECTION_CHECK" => "N",
		"ONLY_ON_VOTING" => "Y"
	)
);?>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
