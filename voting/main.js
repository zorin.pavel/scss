$(document).ready(function() {

    $("#search").click(
		function(){
			sendAjaxForm('result_form', 'ajax_form', '/template/ajax/search.php');
			return false; 
		}
	);

 
function sendAjaxForm(result_form, ajax_form, url) {
	var sear = $("#inps").val();
    $.ajax({
        url:     url, //url страницы (action_ajax_form.php)
        type:     "POST", //метод отправки
        dataType: "text", //формат данных
        data: {'full_name':sear},
        success: function(data) { //Данные отправлены успешно
        	
        	$('#result_form').html(data).fadeIn();
			$('.popup-open').click();
    	},
    	error: function(data) { // Данные не отправлены
            $('#result_form').html('Ошибка. Данные не отправлены.');
    	}
 	});
};

$('.popup-open').click(function() {
		$('.popup-fade').fadeIn();
		return false;
	});	
	
	$('.popup-close').click(function() {
		$(this).parents('.popup-fade').fadeOut();
		return false;
	});		
 
	$(document).keydown(function(e) {
		if (e.keyCode === 27) {
			e.stopPropagation();
			$('.popup-fade').fadeOut();
		}
	});
	
	$('.popup-fade').click(function(e) {
		if ($(e.target).closest('.popup').length == 0) {
			$(this).fadeOut();					
		}
	});
});