<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Победители");
$APPLICATION->SetPageProperty("viewport", "width=device-width, initial-scale=1");

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH ."/css/winners.css");

?>
<div class="banner-container">
	<div class="banner">
		<h1 class="banner-title">Победители ТОП-89</h1>
		<p class="banner-text">Смотри на тех, кто несёт энергию и страсть к достижению целей</p>
	</div>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.filter",
	"template1",
	Array(
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"FIELD_CODE" => array(0=>"NAME",1=>"",),
		"FILTER_NAME" => "arrFilter1",
		"IBLOCK_ID" => "",
		"IBLOCK_TYPE" => "",
		"LIST_HEIGHT" => "5",
		"NUMBER_WIDTH" => "5",
		"PAGER_PARAMS_NAME" => "arrPager",
		"PREFILTER_NAME" => "preFilter",
		"PRICE_CODE" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"SAVE_IN_SESSION" => "N",
		"TEXT_WIDTH" => "20",
		"SHOW_CATEGORIES" => "N",
		"PAGE_ADDRESS" => "/winners"
	)
);
GLOBAL $arrFilter1;
if (!is_array($arrFilter1))
   $arrFilter1 = array();

$categoriesOld = $_GET['winner_categories'];
$categoriesOld = strlen($categoriesOld) > 0 ? explode(',', $categoriesOld) : [];
$categoriesOld = !is_array($categoriesOld) ? [$categoriesOld] : $categoriesOld;

$yearsOld = $_GET['winner_years'];
$yearsOld = strlen($yearsOld) > 0 ? explode(',', $yearsOld) : [];
	/*
$yearsFilterArr = [];
if (count($yearsOld) > 0) {
	if (count($yearsOld) > 1) {
		$yearsFilterArr['LOGIC'] = "OR";
		foreach ($yearsOld as $yearOld) {
			array_push($yearsFilterArr, [
				"<=DATE_CREATE" => '31.12.'.$yearOld.' 23:59:59',
				">=DATE_CREATE" => '01.01.'.$yearOld.' 00:00:00'
			]);
		}
	} else {
		array_push($yearsFilterArr, [
				"<=DATE_CREATE" => '31.12.'.$yearsOld[0].' 23:59:59',
				">=DATE_CREATE" => '01.01.'.$yearsOld[0].' 00:00:00'
			]);
	}
}

if (count($categoriesOld) > 0) {
	if (count($yearsOld) > 0) {
		$arrFilter1['LOGIC'] = "AND";
		array_push($arrFilter1, $yearsFilterArr);
		array_push($arrFilter1, ['PROPERTY_CATEGORY' => $categoriesOld]);
	} else {
		$arrFilter1['PROPERTY_CATEGORY'] = $categoriesOld;
	}
}
var_dump($arrFilter1);
*/

$arrFilter1['PROPERTY_CATEGORY'] = $categoriesOld;

?>

<div class="winners-container">
	<div class="filter-winners-container">
        <form name="winners_filter_form" action="/winners/" method="get">
		<div class="winners-filter">
			<div class="winners-filter-category filter-category-year">
				<div class="winners-filter-category-title">
					Год участия
                    <svg role="icon" class="caret-down">
                        <use xlink:href='<?=SITE_TEMPLATE_PATH?>/icons/caret-down.svg#icon' href="<?=SITE_TEMPLATE_PATH?>/icons/caret-down.svg#icon"></use>
                    </svg>
				</div>
				<div class="winners-filter-items filter-category-year-items">
					<label class="winners-filter-item-label">
						<input name="winner-filter-year-all" type="checkbox" class="winners-filter-item winners-filter-item-year-all">
						<span>Все</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_years" value="2022" type="checkbox" class="winners-filter-item winners-filter-item-year">
						<span>2022</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_years" value="2021" type="checkbox" class="winners-filter-item winners-filter-item-year">
						<span>2021</span>
					</label>
					<?if(false):?>
                        <label class="winners-filter-item-label">
                            <input name="winner_years" value="2020" type="checkbox" class="winners-filter-item winners-filter-item-year">
                            <span>2020</span>
                        </label>
					<?endif?>
				</div>
			</div>
			<div class="winners-filter-category filter-category-nomination">
				<div class="winners-filter-category-title">
					Номинации
                    <svg role="icon" class="caret-down">
                        <use xlink:href='<?=SITE_TEMPLATE_PATH?>/icons/caret-down.svg#icon' href="<?=SITE_TEMPLATE_PATH?>/icons/caret-down.svg#icon"></use>
                    </svg>
				</div>
				<div class="winners-filter-items filter-category-nomination-items">
					<label class="winners-filter-item-label">
						<input name="winner-filter-category-all" type="checkbox" class="winners-filter-item winners-filter-item-category-all">
						<span>Все</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="11" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Воспитание и образование</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="30" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Дети</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories"  value="4" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Искусство</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="13" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Добровольчество и благотворительность</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="12" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Здоровье и медицина</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="7" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Медиа</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="6" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Мода и дизайн</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="8" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Музыка</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories"  value="5" type="checkbox" class="winners-filter-item winners-filter-item-category1">
						<span>Наука и технологии</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="15" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Национальные традиции</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="3" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Предпринимательство</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="9" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Спорт и киберспорт</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="14" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Труд для людей</span>
					</label>
					<label class="winners-filter-item-label">
						<input name="winner_categories" value="10" type="radio" class="winners-filter-item winners-filter-item-category1">
						<span>Управление</span>
					</label>
				</div>
			</div>
		</div>
    </form>
	</div>

    <div class="winners-list-container">
	 <?

if (count($yearsOld) == 0 | in_array('2022', $yearsOld)) {
	echo '<p class="winners-list-header">Победители 2022 года</p>';

$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"winners",
		Array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"ADD_SECTIONS_CHAIN" => "N",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "3600",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"COMPONENT_TEMPLATE" => "pretendenty",
			"DETAIL_URL" => "/winners/info/?WIN_YEAR=2022&ELEMENT_ID=#ELEMENT_ID#",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"DISPLAY_DATE" => "N",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "N",
			"DISPLAY_TOP_PAGER" => "N",
			"FIELD_CODE" => array(0=>"ID",1=>"CODE",2=>"XML_ID"),
			"FILTER_NAME" => "arrFilter1",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"IBLOCK_ID" => "1",
			"IBLOCK_TYPE" => "persons",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"INCLUDE_SUBSECTIONS" => "Y",
			"MESSAGE_404" => "",
			"NEWS_COUNT" => "500",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => "modern",
			"PAGER_TITLE" => "Новости",
			"PARENT_SECTION" => "55",
			"PARENT_SECTION_CODE" => "",
			"PREVIEW_TRUNCATE_LEN" => "",
			"PROPERTY_CODE" => array(0=>"CATEGORY_NEW"),
			"SET_BROWSER_TITLE" => "N",
			"SET_LAST_MODIFIED" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_STATUS_404" => "Y",
			"SET_TITLE" => "N",
			"SHOW_404" => "N",
			"SORT_BY1" => "UPDATED",
			"SORT_BY2" => "",
			"SORT_ORDER1" => "DESC",
			"SORT_ORDER2" => "",
			"STRICT_SECTION_CHECK" => "N",
			"SHOW_CONTROL_BUTTONS" => "N"
		)
	);
}

if (count($yearsOld) == 0 | in_array('2021', $yearsOld)) {
	echo '<p class="winners-list-header">Победители 2021 года</p>';

$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"winners",
		Array(
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"ADD_SECTIONS_CHAIN" => "N",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "3600",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"COMPONENT_TEMPLATE" => "pretendenty",
			"DETAIL_URL" => "/winners/info/?WIN_YEAR=2021&ELEMENT_ID=#ELEMENT_ID#",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"DISPLAY_DATE" => "N",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "N",
			"DISPLAY_TOP_PAGER" => "N",
			"FIELD_CODE" => array(0=>"ID",1=>"CODE",2=>"XML_ID"),
			"FILTER_NAME" => "arrFilter1",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"IBLOCK_ID" => "1",
			"IBLOCK_TYPE" => "persons",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"INCLUDE_SUBSECTIONS" => "Y",
			"MESSAGE_404" => "",
			"NEWS_COUNT" => "500",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => "modern",
			"PAGER_TITLE" => "Новости",
			"PARENT_SECTION" => "32",
			"PARENT_SECTION_CODE" => "",
			"PREVIEW_TRUNCATE_LEN" => "",
			"PROPERTY_CODE" => array(0=>"CATEGORY_NEW"),
			"SET_BROWSER_TITLE" => "N",
			"SET_LAST_MODIFIED" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_STATUS_404" => "Y",
			"SET_TITLE" => "N",
			"SHOW_404" => "N",
			"SORT_BY1" => "UPDATED",
			"SORT_BY2" => "",
			"SORT_ORDER1" => "DESC",
			"SORT_ORDER2" => "",
			"STRICT_SECTION_CHECK" => "N",
			"SHOW_CONTROL_BUTTONS" => "N"
		)
	);
}

?>
    </div>
</div>

<script>
    $('.winners-filter-category-title').click(function (e) {
        if($(this).next().hasClass('opened'))
            $(this).next().removeClass('opened');
        else
            $(this).next().addClass('opened');
    });
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
