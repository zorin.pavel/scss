// lazy load images
document.addEventListener("DOMContentLoaded", function() {
    let lazyImages = document.querySelectorAll('.lazy');
    let options = {
        root: null, // viewport
        rootMargin: '0px', // Margin around the root
        threshold: 0.5 // Percentage of the target element which should be visible before it's loaded
    };

    let observer = new IntersectionObserver(function(entries, observer) {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                let img = entry.target;
                img.src = img.dataset.src; // Load the image
                observer.unobserve(img); // Unobserve the image after it's loaded
            }
        });
    }, options);

    lazyImages.forEach(image => {
        observer.observe(image);
    });
});



$(function() {
    // категория дети
    if(document.getElementById('categor')) {
        document.getElementById('categor').addEventListener('change', function(e) {
            detidop();
        })

        function detidop() {
            if(document.getElementById('categor').value != 43) {
                document.getElementById('detifio').setAttribute('required', '');
                document.getElementById('detitel').setAttribute('required', '');
                document.getElementById('dettti').style.display = 'none';
            } else {
                document.getElementById('detifio').setAttribute('required', 'required');
                document.getElementById('detitel').setAttribute('required', 'required');
                document.getElementById('dettti').style.display = 'block';
            }
        }

        detidop();
    }

    $('.span-float').click(function() {
        var nnn = $('[name^=\'DOST\']').length + 1;
        if(nnn < 8) {
            $(this).parent('.div-form_label').append('<label class="label-form " for=""><input type="text" name="DOST' + nnn + '" class="input-form dost-' + nnn + ' border"><span class="span-label border">-</span></label');
            $('.span-label').click(function() {
                if($(this).closest('.div-form_label '))
                    $(this).parent('.label-form').css('display', 'none');
            });
        }
        /*if(nnn>3){

            $('.span-float').css('display', 'none');
        };*/
    });
    $('.span-float_foto').click(function() {
        var nn = $('[id^=\'dost-\']').length + 1;
        $(this).parent('.block-foto').append('<label class="label-form_foto label-form foto-first border fileimg dost-' + nn + '" for="dost-' + nn + '"><span class="span-label border">-</span><input type="file" class="fileimg" id ="dost-' + nn + '" onchange="rr(this);"></label>');
        $('.span-label').click(function() {
            $(this).parent('.label-form').css('display', 'none');
        });
    });

    let fotodostCnt = 1;
    $('.plus').click(function() {
        const open = () => {
            fotodostCnt++;

            if($(`#fotodost-${fotodostCnt}`).is(':visible'))
                open();
            else
                $(`#fotodost-${fotodostCnt}`).css('display', 'block');
        }

        open();

        if(fotodostCnt >= 5) {
            $('.plus').css('display', 'none');
        }
    });

    $(window).scroll(function() {
        if($(this).scrollTop() > $('header').height()) {
            $('.left-block_menu').addClass('sticky');
        } else {
            $('.left-block_menu').removeClass('sticky');
        }
    });

    function readURL(input) {
        if(input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#imgInp').change(function() {
        readURL(this);
    });

    /*document.querySelectorAll("input.fileimg").addEventListener("change", function () {
        var idinp=this.id;alert(idinp);
      if (this.files[0]) {

        var fr = new FileReader();

        fr.addEventListener("load", function () {
          document.querySelector("."+idinp).style.backgroundImage = "url(" + fr.result + ")";
        }, false);

        fr.readAsDataURL(this.files[0]);
      }
    });	*/

});

const toggleRules = (id) => {
    if($(`#${id}`).hasClass('open'))
        $(`#${id}`).removeClass('open');
    else
        $(`#${id}`).addClass('open');
}

const toggleMenu = (oc) => {
    oc ?
        $('.menu').addClass('open') :
        $('.menu').removeClass('open');
};

function updatePhoto(elem) {
    const pp = $(elem).parent()[0];
    const fr = new FileReader();

    fr.addEventListener('load', function() {
        $(pp).css('backgroundImage', `url("${fr.result}")`);
    }, false);

    fr.readAsDataURL(elem.files[0]);
}

function updateFotodost(elem, num = 0) {
    const idinp = elem.id;
    const fileTypes = ['jpg', 'jpeg', 'png'];

    Array.from(elem.files).forEach(file => {
        const fr = new FileReader();

        const extension = file.name.split('.').pop().toLowerCase();
        const isImage = fileTypes.indexOf(extension) > -1;

        fr.addEventListener('load', function() {
            $(`#files-icons${num}`)
                .append(`
                    <div class="files-icon">
                        <img width="24" src="/upload/medialibrary/5b0/k5lq7mk2qbggk6kupp39hh67qz7m3x2y.png" height="24">
                        <p class="files-icon-title">${file.name}</p>
                    </div>
                `);
//            if(isImage) {
//                $("."+idinp).css('backgroundImage', `url("${fr.result}")`);
//                $("."+idinp).css('backgroundSize', 'contain');
//            }
        }, false);

        fr.readAsDataURL(file);
    });
}

function clk_sv(idinp) {
    var elem = document.getElementsByClassName('chek_sv');
    for(i = 0; i < elem.length; i++) {
        if(elem[i].id != idinp) {
            elem[i].checked = '';
        } else {
            elem[i].checked = 'checked';
        }
    }
}

function clos() {
    $('#result_form').removeClass('dwiz');
    $('#result_form_uch').removeClass('dwiz');
    var goal = self.location;
    location.href = goal;
};

function closModer() {
    $('#result_form').removeClass('dwiz');
    $('#result_form_uch').removeClass('dwiz');
};

$(function() {
    $(document).on('click', '.clk_save', function(e) {
        e.preventDefault();
        var $this = $('#ajax_form_uchsnik');
        var $form = $this.closest('form');
        var formUrl = '/ajax/ajax_form_uchsnik.php';
        var countdost = $('[name^=\'DOST\']').length;
        $('#countinp').val(countdost);

        $.ajax({
            url: formUrl,
            method: 'post',
            type: 'POST',             // Type of request to be send, called as method
            data: new FormData($form.get(0)), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData: false,        // To send DOMDocument or non processed data file it is set to false

            dataType: 'json',
            success: function(res) {
                console.log('ok');
                console.log(res);
                var ttext = JSON.stringify(res.msg);
                $('#result_form_uch').addClass('dwiz');
                if(res.no != 'no') {
                    $('#result_form_uch').html('<div class="result_form-wrapper"><p class="tittle-name_block">Все сохранилось!</br>Можешь продолжить работу позднее.</br></br>Мы уже скучаем;)</p><button class="secondary" onclick="clos();">Закрыть</button></div>');
                } else {
                    $('#result_form_uch').html('<div class="result_form-wrapper"><p class="tittle-name_block">' + ttext.replace(/\"/g, '') + '</p><button class="secondary" onclick="closModer();">Ок</button></div>');
                }

                if(!res.hasError) {
                    //  $form.find('input, textarea').val('');
                    //   $('.ctrFiles').html('');
                }
            },
            error: function(p1, p2, p3) {
                console.log(p1);
                console.log(p2);
                console.log(p3);
            }
        });

        return false;
    });

    /* var arFiles = [];
     $(document).on('change', '#dost-1', function(e) {
         var $this = $(this);
         var $ctrFiles = $('.ctrFiles');
         var value = $this.val();
         var fileCnt = $ctrFiles.find('.item').length;

         if(fileCnt >=3) {
             alert('Вы можете добавить не более 3 файлов');
             return false;
         }

         if (value && !arFiles.includes(value)) {
             arFiles.push(value);
             $ctrFiles.append($('<div class="item"><div class="cls"></div>'+value+'</div>').append($this.clone().attr('id', 'att_'+Date.now()).attr('name', 'attach[]')));

         }

     });*/

    $(document).on('click', '.ctrFiles .item .cls', function(e) {
        $(this).closest('.item').remove();
    });

});
////на модерацию
/*
$( document ).ready(function() {
    $(".moder").click(
		function(){
			sendAjaxFormm('result_form', 'ajax_form', '/ajax/ajax_moder.php');
			return false;
		}
	);
});

function sendAjaxFormm(result_form, ajax_form, url) {

    $.ajax({
        url:     url, //url страницы (action_ajax_form.php)
        type:     "POST", //метод отправки
        dataType: "html", //формат данных
        data:{ 'itemId':$("#userid").val()},
        success: function(response) { //Данные отправлены успешно
        	result = $.parseJSON(response);
			$('#result_form_uch').addClass('dwiz');
					//setInterval(function(){
					//	location.reload();
					//}, 3000);

        	$('#result_form_uch').html('<p class="tittle-name_block">Отправлен на модерацию<div class="close" onclick="clos();"></div></p>');
    	},
    	error: function(response) { // Данные не отправлены
            $('#result_form').html('Ошибка. Данные не отправлены.');
    	}
 	});
}

*/

$(function() {
    $(document).on('click', '.moder', function(e) {
        e.preventDefault();
        var $this = $('#ajax_form_uchsnik');
        var $form = $this.closest('form');
        var formUrl = '/ajax/ajax_moder.php';
        var countdost = $('[name^=\'DOST\']').length;
        $('#countinp').val(countdost);
        $.ajax({
            url: formUrl,
            method: 'post',
            type: 'POST',             // Type of request to be send, called as method
            data: new FormData($form.get(0)), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData: false,        // To send DOMDocument or non processed data file it is set to false

            dataType: 'json',
            success: function(res) {
                console.log('ok');
                console.log(res);
                if(res.msg) {
                    $('#result_form_uch').addClass('dwiz');
                    //setInterval(function(){
                    //	$('#result_form_uch').removeClass('dwiz');
                    //}, 5000);
                    var ttext = JSON.stringify(res.msg);
                    if(res.no != 'no') {
                        $('#result_form_uch').html('<div class="result_form-wrapper"><p class="tittle-name_block">Ура, теперь мы познакомимся с тобой!<br>Возникнут вопросы, мы напишем в Чате.<br>Если всё супер, прилетит письмо на почту.<br><br>На связи.</p><button class="secondary" onclick="clos();">Закрыть</button></div>');
                    } else {
                        $('#result_form_uch').html('<div class="result_form-wrapper"><p class="tittle-name_block">' + ttext.replace(/\"/g, '') + '</p><button class="secondary" onclick="closModer();">Ок</button></div>');
                    }
                }

                if(!res.hasError) {
                    //  $form.find('input, textarea').val('');
                    //   $('.ctrFiles').html('');
                }
            },
            error: function(p1, p2, p3) {
                console.log(p1);
                console.log(p2);
                console.log(p3);
            }
        });

        return false;
    });

    /* var arFiles = [];
     $(document).on('change', '#dost-1', function(e) {
         var $this = $(this);
         var $ctrFiles = $('.ctrFiles');
         var value = $this.val();
         var fileCnt = $ctrFiles.find('.item').length;

         if(fileCnt >=3) {
             alert('Вы можете добавить не более 3 файлов');
             return false;
         }

         if (value && !arFiles.includes(value)) {
             arFiles.push(value);
             $ctrFiles.append($('<div class="item"><div class="cls"></div>'+value+'</div>').append($this.clone().attr('id', 'att_'+Date.now()).attr('name', 'attach[]')));

         }

     });*/

    $(document).on('click', '.ctrFiles .item .cls', function(e) {
        $(this).closest('.item').remove();
    });

});
