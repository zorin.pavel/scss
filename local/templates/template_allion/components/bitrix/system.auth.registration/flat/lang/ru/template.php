<?
$MESS["AUTH_REGISTER"] = "регистрация";
$MESS["AUTH_NAME"] = "имя";
$MESS["AUTH_LAST_NAME"] = "фамилия";
$MESS["AUTH_LOGIN_MIN"] = "логин (минимум 3 символа)";
$MESS["AUTH_CONFIRM"] = "подтверждение пароля";
$MESS["CAPTCHA_REGF_PROMT"] = "введите слово на картинке";
$MESS["AUTH_REQ"] = "обязательные поля.";
$MESS["AUTH_AUTH"] = "авторизация";
$MESS["AUTH_PASSWORD_REQ"] = "пароль";
$MESS["AUTH_EMAIL_WILL_BE_SENT"] = "на указанный в форме e-mail придет запрос на подтверждение регистрации.";
$MESS["AUTH_EMAIL_SENT"] = "на указанный в форме e-mail было выслано письмо с информацией о подтверждении регистрации.";
$MESS["AUTH_EMAIL"] = "E-Mail";
$MESS["AUTH_SECURE_NOTE"] = "перед отправкой формы пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["main_register_sms_code"] = "код подтверждения из СМС";
$MESS["main_register_sms_send"] = "отправить";
$MESS["main_register_phone_number"] = "номер телефона";
?>