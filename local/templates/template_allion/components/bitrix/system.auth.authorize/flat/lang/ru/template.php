<?
$MESS["AUTH_AUTH"] = "Вход";
$MESS["AUTH_REG"] = "Регистрация";
$MESS["AUTH_PLEASE_AUTH"] = "Авторизуйся в личном кабинете";
$MESS["AUTH_LOGIN"] = "логин";
$MESS["AUTH_PASSWORD"] = "пароль";
$MESS["AUTH_REMEMBER_ME"] = "запомнить меня на этом компьютере";
$MESS["AUTH_AUTHORIZE"] = "Войти";
$MESS["AUTH_REGISTER"] = "Зарегистрироваться";
$MESS["AUTH_FIRST_ONE"] = "если впервые на сайте, заполни, пожалуйста, регистрационную форму.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Не помню пароль";
$MESS["AUTH_CAPTCHA_PROMT"] = "введите слово на картинке";
$MESS["AUTH_TITLE"] = "войти на сайт";
$MESS["AUTH_SECURE_NOTE"] = "перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"] = "пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
?>