<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH ."/css/login.css");
?>

    <div class="sub-main">
        <div class="background">
            <div class="box">
                <h3><?=GetMessage("AUTH_PLEASE_AUTH")?></h3>

<?
    if(!empty($arParams["~AUTH_RESULT"])) {
        $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
        ?><div class="alert alert-danger"><?=nl2br(htmlspecialcharsbx($text))?></div><?
    }
    if($arResult['ERROR_MESSAGE'] <> '') {
        $text = str_replace(array("<br>", "<br />"), "\n", $arResult['ERROR_MESSAGE']);
        ?><div class="alert alert-danger"><?=nl2br(htmlspecialcharsbx($text))?></div><?
    }

    if($arResult["AUTH_SERVICES"]) {
        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form",
            "flat",
            array(
                "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                "AUTH_URL" => $arResult["AUTH_URL"],
                "POST" => $arResult["POST"],
            ),
            $component,
            array("HIDE_ICONS"=>"Y")
        );
    }
?>

                <div class="button-group">
                    <button id="button_AUTH" onclick="setForm('AUTH')"><?=GetMessage("AUTH_AUTH")?></button>
                    <button id="button_REGISTRATION" onclick="setForm('REGISTRATION')" class="flat"><?=GetMessage("AUTH_REG")?></button>
                </div>

                <form name="AUTH" method="post" action="<?=$arResult["AUTH_URL"]?>">
                    <input type="hidden" name="AUTH_FORM" value="Y" />
                    <input type="hidden" name="TYPE" value="AUTH" />
                    <?if($arResult["BACKURL"] <> ''):?>
                        <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
                    <?endif?>
                    <?foreach($arResult["POST"] as $key => $value):?>
                        <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
                    <?endforeach?>

                    <input type="email" name="USER_LOGIN" maxlength="255" value="<?=$arResult["LAST_LOGIN"]?>" placeholder="E-mail"/>
                    <input type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" placeholder="Пароль" />

                    <?if($arResult["CAPTCHA_CODE"]):?>
                            <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />

                            <div class="bx-authform-formgroup-container dbg_captha">
                                <div class="bx-authform-label-container">
                                    <?echo GetMessage("AUTH_CAPTCHA_PROMT")?>
                                </div>
                                <div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
                                <div class="bx-authform-input-container">
                                    <input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" />
                                </div>
                            </div>
                    <?endif;?>

                    <button type="submit"><?=GetMessage("AUTH_AUTHORIZE")?></button>
                    <a role="button" class="flat" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
                </form>


                <form name="REGISTRATION" method="post" action="<?=$arResult["AUTH_URL"]?>" style="display:none">
                    <input type="hidden" name="AUTH_FORM" value="Y" />
                    <input type="hidden" name="TYPE" value="REGISTRATION" />
					<input type="hidden" name="UF_AUTH_FROM_DETI" value="<?= $_GET["selected_category"] == 'top_deti' ? 'Y' : 'N'?>" />
                    <input type="hidden" id="email-reg-input" name="USER_EMAIL" value="" />
					<input type="hidden" id="name-reg-input" name="USER_NAME" value="" />
					<input type="hidden" id="lastname-reg-input" name="USER_LASTNAME" value="" />
					<input type="hidden" id="surname-reg-input" name="USER_SURNAME" value="" />

                    <input type="text" id="user-fio-reg" name="USER_FIO" maxlength="255" value="<?=$arResult["USER_FIO"]?>" placeholder="Ф.И.О." />
                    <input type="email" id="user-login-reg" name="USER_LOGIN" maxlength="255" value="<?=$arResult["USER_LOGIN"]?>" placeholder="E-mail" />
                    <input type="password" name="USER_PASSWORD" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off" placeholder="Пароль" />
                    <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off" placeholder="Повторите пароль" />

                    <?if($arResult["CAPTCHA_CODE"]):?>
                            <input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />

                            <div class="bx-authform-formgroup-container dbg_captha">
                                <div class="bx-authform-label-container">
                                    <?echo GetMessage("AUTH_CAPTCHA_PROMT")?>
                                </div>
                                <div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
                                <div class="bx-authform-input-container">
                                    <input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off" />
                                </div>
                            </div>
                    <?endif;?>

                    <button type="submit"><?=GetMessage("AUTH_REGISTER")?></button>
                </form>
            </div>
        </div>
    </div>





	<div class="top-rules-auth" style="display:none">
        <h3 class="rules-header">Правила участия</h3>

		<div class="rules-section" id="rules-section-0">
			<div class="rules-section-header" onclick="toggleRules('rules-section-0')">
				<h4 class="rules-section-header-text">Методология премии</h4>
                <svg role="icon" class="icon-plus">
                    <use xlink:href='<? echo SITE_TEMPLATE_PATH; ?>/icons/plus.svg#icon' href="<? echo SITE_TEMPLATE_PATH; ?>/icons/plus.svg#icon"></use>
                </svg>
                <svg role="icon" class="icon-close">
                    <use xlink:href='<? echo SITE_TEMPLATE_PATH; ?>/icons/cross.svg#icon' href="<? echo SITE_TEMPLATE_PATH; ?>/icons/cross.svg#icon"></use>
                </svg>
			</div>
			<div class="rules-subsection">
				<h5 class="rules-subsection-header-text">Регистрация</h5>
				<p class="rules-subsection-text">Чтобы принять участие в рейтинге, создай личный кабинет. Заполни анкету и укажи значимые достижения за последние полтора года.</p>
			</div>
			<div class="rules-subsection">
				<h5 class="rules-subsection-header-text">Критерии</h5>
				<p class="rules-subsection-text">Заявку могут подать участники в возрасте от 18 до 35 лет. Кандидаты в возрасте от 7 до 17 лет оцениваются в специальной номинации «ТОП-дети». Оценка осуществляется согласно <a href="https://xn--89-dlcaps4dk1f.xn--p1ai/polozhenie2024.pdf" target="_blank">положению</a>. Приоритет отдаётся тем заявкам, где достижения подтверждены фотографиями и сканами документов.</p>
			</div>
			<div class="rules-subsection">
				<h5 class="rules-subsection-header-text">Публикация профилей</h5>
				<p class="rules-subsection-text">В шорт-лист входят 200 самых качественно заполненных заявок с подтверждением достижений. Далее они участвуют в народном голосовании, оценке жюри и взаимооценке.</p>
			</div>
			<div class="rules-subsection">
				<h5 class="rules-subsection-header-text">Истории успеха</h5>
				<p class="rules-subsection-text">Каждый участник рейтинга — герой уникальной истории. Мы поможем рассказать её, найти поддержку и единомышленников.</p>
			</div>
			<div class="rules-subsection">
				<h5 class="rules-subsection-header-text">Экспертная оценка</h5>
				<p class="rules-subsection-text">Жюри рейтинга оценивает всех финалистов по их значимости и вкладу в общее дело.</p>
			</div>
			<div class="rules-subsection">
				<h5 class="rules-subsection-header-text">Народное голосование</h5>
				<p class="rules-subsection-text">Голосование осуществляется через VK Mini Apps и идёт в параллель с перекрёстной и экспертной оценками с 3 по 14 июня.</p>
			</div>
			<div class="rules-subsection">
				<h5 class="rules-subsection-header-text">Перекрёстное голосование</h5>
				<p class="rules-subsection-text">Каждый номинант сможет анонимно оценить пятерых участников, с которыми борется за место в ТОПе.</p>
			</div>
			<div class="rules-subsection">
				<h5 class="rules-subsection-header-text">Награждение</h5>
				<p class="rules-subsection-text">Имена всех участников и победителей будут озвучены на торжественной церемонии 29 июня и опубликованы на сайте.</p>
			</div>
		</div>

		<div class="rules-section" id="rules-section-1">
			<div class="rules-section-header" onClick="toggleRules('rules-section-1')">
				<h4 class="rules-section-header-text">Правила заполнения анкеты</h4>
                <svg role="icon" class="icon-plus">
                    <use xlink:href='<? echo SITE_TEMPLATE_PATH; ?>/icons/plus.svg#icon' href="<? echo SITE_TEMPLATE_PATH; ?>/icons/plus.svg#icon"></use>
                </svg>
                <svg role="icon" class="icon-close">
                    <use xlink:href='<? echo SITE_TEMPLATE_PATH; ?>/icons/cross.svg#icon' href="<? echo SITE_TEMPLATE_PATH; ?>/icons/cross.svg#icon"></use>
                </svg>
			</div>
			<div class="rules-subsection">
				<ul class="rules-subsection-list">
					<li>
						<p class="rules-subsection-list-item">Прежде чем приступить к заполнению профиля, внимательно изучи методологию премии и <a href="https://xn--89-dlcaps4dk1f.xn--p1ai/polozhenie2024.pdf" target="_blank">положение</a> — это поможет тебе лучше понять, какие моменты важно отразить.</p>
					</li>
					<li>
						<p class="rules-subsection-list-item">Заполни основные  поля, но не забудь обратить внимание на необязательные. Они могут сделать твой профиль более интересным и запоминающимся.</p>
					</li>
					<li>
						<p class="rules-subsection-list-item">Используй кнопку «Сохранить». Это позволит продолжить заполнение позже. Советуем сохраняться после каждого пункта, чтобы при обновлении страницы данные не потерялись.</p>
					</li>
					<li>
						<p class="rules-subsection-list-item">Пиши грамотно. Мы сохраняем оригинальную орфографию и пунктуацию, поэтому ответственность за текст лежит на тебе.</p>
					</li>
					<li>
						<p class="rules-subsection-list-item">Выбери свою лучшую фотографию для блока «Фотографии». Она будет главной на сайте. Добавь другие снимки, иллюстрирующие твои достижения.</p>
					</li>
					<li>
						<p class="rules-subsection-list-item">Подробно опиши свои достижения за последние полтора года. На них будут ориентироваться жюри, эксперты, зрители.</p>
					</li>
					<li>
						<p class="rules-subsection-list-item">После завершения нажми кнопку «Отправить заявку на модерацию». Дальнейшие изменения будут невозможны. Мы проверим и опубликуем твой профиль на сайте.</p>
					</li>
					<li>
						<p class="rules-subsection-list-item">Убедись, что твои аккаунты в соцсетях видны не только друзьям. Это поможет проходить модерацию.</p>
					</li>
					<li>
						<p class="rules-subsection-list-item">Кнопка «Поделиться» станет активной, когда твой профиль пройдет модерацию и появится на сайте. Тогда ты сможешь поделиться им в социальных сетях.</p>
					</li>
					<li>
						<p class="rules-subsection-list-item">Подпишись на нас <a href="https://vk.com/top.yamal" target="_blank">ВКонтакте</a>, чтобы быть в курсе актуальных новостей.</p>
					</li>
				</ul>
			</div>
		</div>
	</div>



<script type="text/javascript">
<?if ($arResult["LAST_LOGIN"] <> ''):?>
    try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?else:?>
    try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<?endif?>

$("#user-login-reg").on('change keydown paste input', function(){
    $('#email-reg-input').val($(this).val());
});

$("#user-fio-reg").on('change keydown paste input', function(){
	var vals = $(this).val();
	vals = vals ? vals : '';
	var valsArr = vals.split(' ');
	$('#name-reg-input').val((valsArr.length > 1) ? valsArr[1] : '');
	$('#lastname-reg-input').val(valsArr[0]);
	$('#surname-reg-input').val((valsArr.length > 2) ? valsArr[2] : '');
});

const setForm = (formName) => {
    $(`form`).hide();
    $(`form[name*="${formName}"]`).show();

    $(`button[id*="button_"]`).addClass('flat');
    $(`button[id="button_${formName}"]`).removeClass('flat');

    if(formName === 'REGISTRATION')
        $('.top-rules-auth').css('display', 'block');

    return false;
};
</script>
