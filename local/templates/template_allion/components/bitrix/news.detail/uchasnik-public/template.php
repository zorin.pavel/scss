<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH ."/css/public.css");

$this->setFrameMode(true);

function getYearsToNow($from) {

    $from = new DateTime($from);
    $to = new DateTime();
    $interval = $from->diff($to);
    $diff = intval($interval->format('%y'));
    return $diff.' '.getDeclNum($diff, array('год', 'года', 'лет'));
}

function ifContestTime() {
    $from = new DateTime();
	return $from > (new DateTime('2024-06-03 00:00:00')) && $from < (new DateTime('2024-06-14 00:00:00'));
}

function getDeclNum($value=1, $status= array('','а','ов'))
{
	$array =array(2,0,1,1,1,2);
	return $status[($value%100>4 && $value%100<20)? 2 : $array[($value%10<5)?$value%10:5]];
}
$bdf = $arResult["PROPERTIES"]["BIRTH_DATE"]["VALUE"];
$vzf = $arResult["PROPERTIES"]["VOZRAST"]["VALUE"];
$yearStr = $bdf ? getYearsToNow($arResult["PROPERTIES"]["BIRTH_DATE"]["VALUE"]) : $vzf.' '.getDeclNum($vzf, array('год', 'года', 'лет'));

?>
<?
$arGroupAvalaible = array("9"); // массив групп, которые в которых нужно проверить доступность пользователя
$arGroups = CUser::GetUserGroup($USER->GetID()); // массив групп, в которых состоит пользователь
$result_intersect = array_intersect($arGroupAvalaible, $arGroups);// далее проверяем, если пользователь вошёл хотя бы в одну из групп, то позволяем ему что-либо делать
?>
<div class="news-detail">
	<div class="person-share-section">
        <button id="share-btn" type="button" class="flat">
            Поделиться
            <svg role="icon">
                <use xlink:href='<? echo SITE_TEMPLATE_PATH; ?>/icons/share.svg#icon' href="<? echo SITE_TEMPLATE_PATH; ?>/icons/share.svg#icon"></use>
            </svg>
        </button>
	</div>

	<div class="person-profile-section">
		<div class="profile-section-left">
			<div class="profile-section-block-splited">
				<div class="block-subsection-left">
					<img class="person-img" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" />
					<?if($arResult["PROPERTIES"]["LIGHTENED"]["VALUE"] || $arResult['PROPERTIES']['ZAY']['VALUE_ENUM_ID']==20):?>
                    <div class="person-lightened">
                        <div class="person-lightened-text-container">
                            <p class="person-lightened-text">Меня высветили</p>
                        </div>
                        <svg role="icon button" class="person-lightened-img">
                            <use xlink:href='<?=SITE_TEMPLATE_PATH?>/icons/sun.svg#icon' href="<?=SITE_TEMPLATE_PATH?>/icons/sun.svg#icon"></use>
                        </svg>
                    </div>
					<?endif?>
				</div>
				<div class="block-subsection-right">
					<div class="profile-bio">
						<h3 class="person-fio"><?echo $arResult["NAME"]?></h3>
						<?if($arResult["PROPERTIES"]["CATEGORY_NEW"]["VALUE_ENUM_ID"]):?>
						<p class="person-text"><?=$arResult["PROPERTIES"]["CATEGORY_NEW"]["VALUE_ENUM_ID"] == "42" ? "Знаю ТОЛК" : ($arResult["PROPERTIES"]["CATEGORY_NEW"]["VALUE_ENUM_ID"] == "43" ? "Топ-дети" : "Хочу в ТОП-89") ?></p>
						<?endif?>
						<?if(isset($_GET['WIN_YEAR'])):?>
						<p class="person-text">Год участия: <?=$_GET['WIN_YEAR']?></p>
						<?endif?>
					</div>
					<div class="profile-bio-icons">
						<div class="bio-icon-item">
                            <svg role="icon">
                                <use xlink:href='<?=SITE_TEMPLATE_PATH?>/icons/user.svg#icon' href="<?=SITE_TEMPLATE_PATH?>/icons/user.svg#icon"></use>
                            </svg>
							<p class="bio-icon-text"><?echo $yearStr ?> </p>
						</div>
						<div class="bio-icon-item">
                            <svg role="icon">
                                <use xlink:href='<?=SITE_TEMPLATE_PATH?>/icons/location.svg#icon' href="<?=SITE_TEMPLATE_PATH?>/icons/location.svg#icon"></use>
                            </svg>
							<p class="bio-icon-text"><?=$arResult["PROPERTIES"]["GDEGIVESH"]["VALUE"]?></p>
						</div>
						<div class="bio-icon-item social">
							<?if($arResult["PROPERTIES"]["VK"]["VALUE"]):?>
							<a href="<?=$arResult["PROPERTIES"]["VK"]["VALUE"]?>" target="_blank"><img src='<?=SITE_TEMPLATE_PATH?>/icons/vk.svg#icon' /></a>
							<?endif?>
							<?if($arResult["PROPERTIES"]["TELEGRAM"]["VALUE"]):?>
							<a href="https://t.me/<?=preg_replace('/@/', '', $arResult["PROPERTIES"]["TELEGRAM"]["VALUE"])?>" target="_blank">
                            <img src='<?=SITE_TEMPLATE_PATH?>/icons/telegram.svg#icon' /></a>
							<?endif?>
							<?if($arResult["PROPERTIES"]["OK"]["VALUE"]):?>
							<a href="<?=$arResult["PROPERTIES"]["OK"]["VALUE"]?>" target="_blank">
                            <img src='<?=SITE_TEMPLATE_PATH?>/icons/ok.svg#icon' /></a>
							<?endif?>
						</div>
					</div>
					<?if(ifContestTime() && arParams["SHOW_VOTE_BTN"] != "N"):?>
                    	<button type="button" class="secondary">Проголосовать</button>
					<?endif?>
				</div>
			</div>
			<div class="profile-section-block">
				<div class="block-subsection">
					<?if(!empty($result_intersect)):?>
					<? //var_dump($arResult)?>
						<div class="profile-history-title">
							<h4 class="profile-history-title-text">Поля для модераторов</h4>
						</div>
						<div class="profile-history-block">
							<h5 class="profile-history-block-title">Связь с Ямалом</h5>
							<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["SVYAZ"]["VALUE_ENUM"][0])?></p>
						</div>
						<div class="profile-history-block">
							<h5 class="profile-history-block-title">Почта участника</h5>
							<p class="profile-history-block-text"><?=$arResult["PROPERTIES"]["EMAIL"]["VALUE"] ? htmlspecialchars_decode($arResult["PROPERTIES"]["EMAIL"]["VALUE"]) : "Не указано"?></p>
						</div>
						<div class="profile-history-block">
							<h5 class="profile-history-block-title">Телефон участника</h5>
							<p class="profile-history-block-text"><?=$arResult["PROPERTIES"]["TELEFON"]["VALUE"] ? htmlspecialchars_decode($arResult["PROPERTIES"]["TELEFON"]["VALUE"]) : "Не указано"?></p>
						</div>
						<div class="profile-history-block">
							<h5 class="profile-history-block-title">Место работы/учёбы</h5>
							<p class="profile-history-block-text"><?=$arResult["PROPERTIES"]["MESTO"]["VALUE"] ? htmlspecialchars_decode($arResult["PROPERTIES"]["MESTO"]["VALUE"]) : "Не указано"?></p>
						</div>
						<div class="profile-history-block">
							<h5 class="profile-history-block-title">Видеовизитка</h5>
							<p class="profile-history-block-text"><?=$arResult["PROPERTIES"]["VIDEO_CARD"]["VALUE"] ? htmlspecialchars_decode($arResult["PROPERTIES"]["VIDEO_CARD"]["VALUE"]) : "Не указано"?></p>
						</div>
						<?if(!empty($arResult["PROPERTIES"]["FINAL_YEAR"]["VALUE"])):?>
							<div class="profile-history-block">
								<h5 class="profile-history-block-title">Финалист какого сезона</h5>
								<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["FINAL_YEAR"]["VALUE"])?></p>
							</div>
						<?endif?>
						<?if(!empty($arResult["PROPERTIES"]["FIODETI"]["VALUE"])):?>
							<div class="profile-history-block">
								<h5 class="profile-history-block-title">ФИО законного представителя</h5>
								<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["FIODETI"]["VALUE"])?></p>
							</div>
						<?endif?>
						<?if(!empty($arResult["PROPERTIES"]["PARENT_BIRTHDATE"]["VALUE"])):?>
							<div class="profile-history-block">
								<h5 class="profile-history-block-title">Дата рождения законного представителя</h5>
								<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["PARENT_BIRTHDATE"]["VALUE"])?></p>
							</div>
						<?endif?>
						<?if(!empty($arResult["PROPERTIES"]["PARENT_EMAIL"]["VALUE"])):?>
							<div class="profile-history-block">
								<h5 class="profile-history-block-title">Email законного представителя</h5>
								<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["PARENT_EMAIL"]["VALUE"])?></p>
							</div>
						<?endif?>
						<?if(!empty($arResult["PROPERTIES"]["TELEFONDETI"]["VALUE"])):?>
							<div class="profile-history-block">
								<h5 class="profile-history-block-title">Телефон законного представителя</h5>
								<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["TELEFONDETI"]["VALUE"])?></p>
							</div>
						<?endif?>
						<?if(!empty($arResult["PROPERTIES"]["PARENT_WORKPLACE"]["VALUE"])):?>
							<div class="profile-history-block">
								<h5 class="profile-history-block-title">Место работы законного представителя</h5>
								<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["PARENT_WORKPLACE"]["VALUE"])?></p>
							</div>
						<?endif?>
						<?if(!empty($arResult["PROPERTIES"]["VK"]["VALUE"])):?>
							<div class="profile-history-block">
								<h5 class="profile-history-block-title">VK законного представителя</h5>
								<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["VK"]["VALUE"])?></p>
							</div>
						<?endif?>
						<?if(!empty($arResult["PROPERTIES"]["OK"]["VALUE"])):?>
							<div class="profile-history-block">
								<h5 class="profile-history-block-title">ОК законного представителя</h5>
								<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["OK"]["VALUE"])?></p>
							</div>
						<?endif?>
						<?if(!empty($arResult["PROPERTIES"]["TELEGRAM"]["VALUE"])):?>
							<div class="profile-history-block">
								<h5 class="profile-history-block-title">Telegram законного представителя</h5>
								<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["TELEGRAM"]["VALUE"])?></p>
							</div>
						<?endif?>
						<div class="profile-history-block">
							<h5 class="profile-history-block-title">Размер одежды</h5>
							<p class="profile-history-block-text"><?=$arResult["PROPERTIES"]["CLOTH_SIZE"]["VALUE"] ? htmlspecialchars_decode($arResult["PROPERTIES"]["CLOTH_SIZE"]["VALUE"]) : "Не указано"?></p>
						</div>
						<div class="profile-history-block">
							<h5 class="profile-history-block-title">Файлы достижений</h5>
							<div class="files-icons" id="files-icons0">
								<?	
									$ii = 0;
									foreach($arResult["PROPERTIES"]["FOTODOST"]["VALUE"] as $fileId) {
										$fileDost = CFile::GetFileArray($fileId);
										?>
	
												<div class="files-icon" style="display: flex; gap: 0.5rem; align-items: center;">
													<a target="_blank" href="<?=$fileDost["SRC"]?>">
													<img width="24" src="/upload/medialibrary/5b0/k5lq7mk2qbggk6kupp39hh67qz7m3x2y.png" height="24">
													</a>
													<a target="_blank" href="<?=$fileDost["SRC"]?>">
													<p class="files-icon-title"><?= $fileDost["ORIGINAL_NAME"] ?></p>
													</a>
												</div>
												<br/>
										<?
									}
									if ($ii == 0) {
										echo 'Не прикреплены';
									}
								?>
							</div>
						</div>
					<?endif?>
					<div class="profile-history-title">
						<h4 class="profile-history-title-text">Моя история</h4>
					</div>
						<?if($arResult["PROPERTIES"]["FOKUS"]["~VALUE"]["TEXT"]):?>
					<div class="profile-history-block">
						<h5 class="profile-history-block-title">Фокус деятельности и достижения</h5>
						<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["FOKUS"]["~VALUE"]["TEXT"])?></p>
					</div>
							<?endif?>
							<?if($arResult["PROPERTIES"]["VOPR"]["VALUE"]):?>
					<div class="profile-history-block">
						<h5 class="profile-history-block-title">Почему я достоин быть в топе?</h5>
						<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["VOPR"]["VALUE"])?></p>
					</div>
							<?endif?>
							<?if($arResult["PROPERTIES"]["YAMAL"]["~VALUE"]["TEXT"]):?>
					<div class="profile-history-block">
						<h5 class="profile-history-block-title">Что для меня Ямал?</h5>
						<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["YAMAL"]["~VALUE"]["TEXT"])?></p>
					</div>
							<?endif?>
							<?if($arResult["PROPERTIES"]["LUBMESTO"]["VALUE"]):?>
					<div class="profile-history-block">
						<h5 class="profile-history-block-title">Моё любимое место на Ямале?</h5>
						<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["LUBMESTO"]["VALUE"])?></p>
					</div>
							<?endif?>
							<?if($arResult["PROPERTIES"]["VOPROS3"]["~VALUE"]["TEXT"]):?>
					<div class="profile-history-block">
						<h5 class="profile-history-block-title">Цитата, отражающая моё мировоззрение</h5>
						<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["VOPROS3"]["~VALUE"]["TEXT"])?></p>
					</div>
							<?endif?>
							<?if($arResult["PROPERTIES"]["VOPROS7"]["~VALUE"]["TEXT"]):?>
					<div class="profile-history-block">
						<h5 class="profile-history-block-title">Мой опыт публичных выступлений</h5>
						<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["VOPROS7"]["~VALUE"]["TEXT"])?></p>
					</div>
							<?endif?>
							<?if($arResult["PROPERTIES"]["VOPROS8"]["~VALUE"]["TEXT"]):?>
					<div class="profile-history-block">
						<h5 class="profile-history-block-title">О чем бы я рассказал аудитории из 150 человек?</h5>
						<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["VOPROS8"]["~VALUE"]["TEXT"])?></p>
					</div>
							<?endif?>
							<?if($arResult["PROPERTIES"]["VOPROS1"]["~VALUE"]["TEXT"]):?>
					<div class="profile-history-block">
						<h5 class="profile-history-block-title">Cобираешься ли уезжать из ЯНАО?</h5>
						<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["VOPROS1"]["~VALUE"]["TEXT"])?></p>
					</div>
							<?endif?>
							<?if($arResult["PROPERTIES"]["VOPROS2"]["~VALUE"]["TEXT"]):?>
					<div class="profile-history-block">
						<h5 class="profile-history-block-title">Любопытные факты о тебе</h5>
						<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["VOPROS2"]["~VALUE"]["TEXT"])?></p>
					</div>
							<?endif?>
							<?if($arResult["PROPERTIES"]["VOPROS4"]["~VALUE"]["TEXT"]):?>
					<div class="profile-history-block">
						<h5 class="profile-history-block-title">Планы на будущее</h5>
						<p class="profile-history-block-text"><?=htmlspecialchars_decode($arResult["PROPERTIES"]["VOPROS4"]["~VALUE"]["TEXT"])?></p>
					</div>
							<?endif?>
				</div>
				<?if(ifContestTime() && arParams["SHOW_VOTE_BTN"] != "N"):?>
                	<button type="button" class="secondary">Проголосовать</button>
				<?endif?>
			</div>
		</div>
		<div class="profile-section-right">
			<div class="profile-section-block">
				<h4 class="profile-facts-title-text">Интересные факты обо мне</h4>
			</div>
			<?if(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST1"]["VALUE"]) || $arResult["PROPERTIES"]["FACTDOST1"]["~VALUE"]["TEXT"]):?>
				<div class="profile-section-block">
					<div class="profile-section-number">
						<p class="profile-number">1</p>
					</div>
					<div class="profile-section-text">
						<p class="profile-fact"><?= $arResult['PROPERTIES']['FOTODOST1']['DESCRIPTION'] ? htmlspecialchars_decode($arResult['PROPERTIES']['FOTODOST1']['DESCRIPTION']) : htmlspecialchars_decode($arResult["PROPERTIES"]["FACTDOST1"]["~VALUE"]["TEXT"])?></p>
					</div>
					<?if(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST1"]["VALUE"])):?>
						<div class="profile-section-img">
							<img class="profile-fact-img" src="<?=CFile::GetPath($arResult["PROPERTIES"]["FOTODOST1"]["VALUE"])?>" />
						</div>
					<?endif?>
				</div>
			<?endif?>
			<?if(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST2"]["VALUE"]) || $arResult["PROPERTIES"]["FACTDOST2"]["~VALUE"]["TEXT"]):?>
				<div class="profile-section-block">
					<div class="profile-section-number">
						<p class="profile-number">2</p>
					</div>
					<div class="profile-section-text">
						<p class="profile-fact"><?= $arResult['PROPERTIES']['FOTODOST2']['DESCRIPTION'] ? htmlspecialchars_decode($arResult['PROPERTIES']['FOTODOST2']['DESCRIPTION']) : htmlspecialchars_decode($arResult["PROPERTIES"]["FACTDOST2"]["~VALUE"]["TEXT"])?></p>
					</div>
					<?if(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST2"]["VALUE"])):?>
						<div class="profile-section-img">
							<img class="profile-fact-img" src="<?=CFile::GetPath($arResult["PROPERTIES"]["FOTODOST2"]["VALUE"])?>" />
						</div>
					<?endif?>
				</div>
			<?endif?>
			<?if(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST3"]["VALUE"]) || $arResult["PROPERTIES"]["FACTDOST3"]["~VALUE"]["TEXT"]):?>
				<div class="profile-section-block">
					<div class="profile-section-number">
						<p class="profile-number">3</p>
					</div>
					<div class="profile-section-text">
						<p class="profile-fact"><?= $arResult['PROPERTIES']['FOTODOST3']['DESCRIPTION'] ? htmlspecialchars_decode($arResult['PROPERTIES']['FOTODOST3']['DESCRIPTION']) : htmlspecialchars_decode($arResult["PROPERTIES"]["FACTDOST3"]["~VALUE"]["TEXT"])?></p>
					</div>
					<?if(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST3"]["VALUE"])):?>
						<div class="profile-section-img">
							<img class="profile-fact-img" src="<?=CFile::GetPath($arResult["PROPERTIES"]["FOTODOST3"]["VALUE"])?>" />
						</div>
					<?endif?>
				</div>
			<?endif?>
			<?if(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST4"]["VALUE"]) || $arResult["PROPERTIES"]["FACTDOST4"]["~VALUE"]["TEXT"]):?>
				<div class="profile-section-block">
					<div class="profile-section-number">
						<p class="profile-number">4</p>
					</div>
					<div class="profile-section-text">
						<p class="profile-fact"><?= $arResult['PROPERTIES']['FOTODOST4']['DESCRIPTION'] ? htmlspecialchars_decode($arResult['PROPERTIES']['FOTODOST4']['DESCRIPTION']) : htmlspecialchars_decode($arResult["PROPERTIES"]["FACTDOST4"]["~VALUE"]["TEXT"])?></p>
					</div>
					<?if(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST4"]["VALUE"])):?>
						<div class="profile-section-img">
							<img class="profile-fact-img" src="<?=CFile::GetPath($arResult["PROPERTIES"]["FOTODOST4"]["VALUE"])?>" />
						</div>
					<?endif?>
				</div>
			<?endif?>
			<?if(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST5"]["VALUE"]) || $arResult["PROPERTIES"]["FACTDOST5"]["~VALUE"]["TEXT"]):?>
				<div class="profile-section-block">
					<div class="profile-section-number">
						<p class="profile-number">5</p>
					</div>
					<div class="profile-section-text">
						<p class="profile-fact"><?= $arResult['PROPERTIES']['FOTODOST5']['DESCRIPTION'] ? htmlspecialchars_decode($arResult['PROPERTIES']['FOTODOST5']['DESCRIPTION']) : htmlspecialchars_decode($arResult["PROPERTIES"]["FACTDOST5"]["~VALUE"]["TEXT"])?></p>
					</div>
					<?if(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST5"]["VALUE"])):?>
						<div class="profile-section-img">
							<img class="profile-fact-img" src="<?=CFile::GetPath($arResult["PROPERTIES"]["FOTODOST5"]["VALUE"])?>" />
						</div>
					<?endif?>
				</div>
			<?endif?>
			<?if(
				!(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST1"]["VALUE"]) || $arResult["PROPERTIES"]["FACTDOST1"]["~VALUE"]["TEXT"]) &&
				!(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST2"]["VALUE"]) || $arResult["PROPERTIES"]["FACTDOST2"]["~VALUE"]["TEXT"]) &&
				!(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST3"]["VALUE"]) || $arResult["PROPERTIES"]["FACTDOST3"]["~VALUE"]["TEXT"]) &&
				!(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST4"]["VALUE"]) || $arResult["PROPERTIES"]["FACTDOST4"]["~VALUE"]["TEXT"]) &&
				!(CFile::GetPath($arResult["PROPERTIES"]["FOTODOST5"]["VALUE"]) || $arResult["PROPERTIES"]["FACTDOST5"]["~VALUE"]["TEXT"])
			):?>
				<div class="profile-section-block">
					<div class="profile-section-text">
						<p class="profile-fact">Я еще не добавил о себе никаких фактов</p>
					</div>
				</div>
			<?endif?>
			</div>
		</div>
	</div>
</div>

<?if(false):?>
<div class="hidden">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h3><?=$arResult["NAME"]?></h3>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif($arResult["DETAIL_TEXT"] <> ''):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	<?foreach($arResult["FIELDS"] as $code=>$value):
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
			}
		}
		else
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
		}
		?><br />
	<?endforeach;
	foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

		<?=$arProperty["NAME"]?>:&nbsp;
		<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
			<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
		<?else:?>
			<?=$arProperty["DISPLAY_VALUE"];?>
		<?endif?>
		<br />
	<?endforeach;
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
</div>
<?endif?>

<script>
const shareButton = document.getElementById('share-btn');

// используем метод addEventListener
shareButton.addEventListener('click', function() {
	navigator.clipboard.writeText('https://высвети89.рф' + "<?=$_SERVER['REQUEST_URI']?>")
	  .then(() => {
		alert('Ссылка скопирована в буфер обмена.');
	  })
	  .catch(err => {
		console.log('Something went wrong', err);
	  });
});
</script>
