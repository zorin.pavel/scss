<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$fio = explode(" ", $arResult['NAME']);
$picture = CFile::ResizeImageGet($arResult['PREVIEW_PICTURE'], array('width' => 494, 'height' => 420), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true );
$pic=0;
?>
<script>
var fioDeti=<?php echo json_encode($arResult["PROPERTIES"]["FIODETI"]["NAME"]);?>;
var fioDetiVal=<?php echo json_encode($arResult["PROPERTIES"]["FIODETI"]["VALUE"]);?>;
var fioDetiCode=<?php echo json_encode($arResult["PROPERTIES"]["FIODETI"]["CODE"]);?>;
</script>
<?
if($arResult['PREVIEW_PICTURE']['SRC']){?>
<style>
.prewf{background-image: url('<?= $picture["src"];?>')!important;background-size: contain;}
</style>
<?}?>	

						
<?
if($arResult['PROPERTIES']['PASPORT']['VALUE']){?>
<?$imgp=CFile::GetPath($arResult['PROPERTIES']['PASPORT']['VALUE']);?>	
<style>
.PASPORTimg{background-image: url('<?= $imgp;?>')!important;background-size: contain!important;}
</style>
<?}?>	
<?
if($arResult['PROPERTIES']['DOCSVYAZ']['VALUE']){?>
<?$imgd=CFile::GetPath($arResult['PROPERTIES']['DOCSVYAZ']['VALUE']);?>	
<style>
.DOCSVYAZimg{background-image: url('<?= $imgd;?>')!important;background-size: contain!important;}
</style>
<?}?>	

<?
foreach($arResult['FOTODOST'] as $el){
	?>
	<style>
.<?=$el['clas']?>{background-image: url('<?=$el['img']?>')!important;background-size: contain!important;}
</style>
	<?
}
?>

<div class="center-column">

				<form runat="server" id="ajax_form_uchsnik" enctype="multipart/form-data" action="/ajax/ajax_form_uchsnik.php">
				<input type="hidden" name="ID" id="userid" value="<?=$arResult['ID']?>">
				<input type="hidden" name="IBLOCKID" value="<?=$arResult['IBLOCK_ID']?>">
				<input type="hidden" name="SECTIONID" value="<?=$arResult['IBLOCK_SECTION_ID']?>">
									<input  type="hidden" name="ZAY" value="<?=$arResult['PROPERTIES']['ZAY']['VALUE_ENUM_ID']?>">
				<input  type="hidden" name="countdoct" id="countinp" value="<?=count($arResult['FOTODOST']);?>">

                    <div class="center-form center-form_first">
     					<h2 class="tittle-form">ЛИЧНЫЙ ПРОФИЛЬ</h2>
     					<div class="div-form">
     						<div class="pswipe-block border">
							<label class="label-form fileimg  prewf prewfot dost-<?echo $pic=$pic+1?>" for="dost-<?echo $pic?>">
							
									<?if(!$arResult['PREVIEW_PICTURE']['SRC']){?>  <!--<p  id="txtf1"><br>Добавьте сюда<br> самую свою <br> удачную фотку</p>--><?}?>
									<input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="prewpict" value="<?= $picture["src"];?>" type="file" class="fileimg inputfile" id ="dost-<?=$pic?>" onchange="rr(this);">
									
								</label>
							

			
                                   </div>
     					</div>
     					<div class="div-form">
     						<p class="tittle-name_block">имя *</p>
     						<label class="label-form " ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="name" class="input-form input-form_first  border" value="<?=$fio[0]?>"></label>
     						<p class="tittle-name_block">фамилия *</p>
     						<label class="label-form " ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="fam" class="input-form  border" value="<?=$fio[1]?>"></label>	
     						<p class="tittle-name_block">возраст *</p>
     						<label class="label-form label-form_age" ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOZRAST" type="text" class="input-form  border" value="<?=$arResult['PROPERTIES']['VOZRAST']['VALUE']?>" required></label>
     					</div>
     					<div class="div-form check-div">
						<?foreach($arResult['SVYAZ'] as $key => $sv){?>
     						<div class="label-form " >
     							<input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="radio" name="SVYAZ<?=$key?>" class="check-input chek_sv" value="<?=$key?>" id="givu<?=$key?>" onclick="clk_sv('givu<?=$key?>');" >	
								<label for="givu<?=$key?>"></label>
     							<span class="tittle-name_block-span"><?=$sv?></span>
     						</div>  						
						<?}?>	
     					</div>
						<div class="div-form">
						    <p class="tittle-name_block">где ты живешь сейчас *</p>
     						<label class="label-form label-form_age" ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?>  name="GDEGIVESH" type="text" class="input-form  border" value="<?=$arResult['PROPERTIES']['GDEGIVESH']['VALUE']?>" required></label>
						</div>
                         <div class="div-form div-form_select">
                         	<p class="tittle-name_block">категория рейтинга *</p>
                         	<select class="select-form border" name="CATEGORY" id="categor">
								<?foreach($arResult['CATEGORY'] as $key=>$el){?>
									<option value="<?=$key?>"><?=$el?></option>
								<?}?>
                         	</select>
							<div id="dettti" <?if(!$arResult["PROPERTIES"]["FIODETI"]["VALUE"]){?>style="display:none;"<?}?>>
							<br>
							<p class="tittle-name_block">информация о законном представителе (родителе) *</p>
							
								<label class="label-form " for=""><input id='detifio' type="text" name="<?=$arResult["PROPERTIES"]["FIODETI"]["CODE"]?>" class="input-form dost- border" value="<?=$arResult["PROPERTIES"]["FIODETI"]["VALUE"]?>" placeholder='ФИО *'></label>
							
								<label class="label-form " for=""><input id='detitel' type="text" name="<?=$arResult["PROPERTIES"]["TELEFONDETI"]["CODE"]?>" class="input-form dost- border" value="<?=$arResult["PROPERTIES"]["TELEFONDETI"]["VALUE"]?>" placeholder='Телефон *'></label>
							
							</div>
                         	<p class="tittle-name_block tittle-name_block-margin">соцсети</p>
                         	<label class="label-form" ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VK" type="text" class="input-form  border" placeholder="ВК" value="<?=$arResult['PROPERTIES']['VK']['VALUE']?>"></label>
                         	<!--<label class="label-form" ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="FACEBOOK" type="text" class="input-form  border" placeholder="FB" value="<?=$arResult['PROPERTIES']['FACEBOOK']['VALUE']?>"></label>
                         	<label class="label-form" ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="INST" type="text" class="input-form  border" placeholder="Instagram" value="<?=$arResult['PROPERTIES']['INST']['VALUE']?>"></label>-->
                         	<!--<label class="label-form" ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="TIKTOK" type="text" class="input-form  border" placeholder="ТikTok" value="<?=$arResult['PROPERTIES']['TIKTOK']['VALUE']?>"></label>-->
                         	<label class="label-form" ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="YOUTUBE" type="text" class="input-form  border" placeholder="YouTube" value="<?=$arResult['PROPERTIES']['YOUTUBE']['VALUE']?>"></label>
                         </div>
     				</div>
     				<div class="center-form">
     					<h2 class="tittle-form">ДОСЬЕ</h2>
     					<div class="div-form">
     						<p class="tittle-name_block">фокус деятельности *</p>
     						<p class="info-form">коротко о том, на что ты делаешь акцент в своей работе, что считаешь целью своей деятельности, на какой результат ориентируешься (для размаха мысли можешь потянуть за уголок, чтобы видно было весь текст сразу, ограничение - не более 500 символов)</p>
                                   <textarea maxlength="500" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="FOKUS" class="input-form border"><?=$arResult['PROPERTIES']['FOKUS']['~VALUE']['TEXT']?> </textarea>
     					</div>
     					<div class="div-form div-form_label">
     						<p class="tittle-name_block">достижения *</p>
     						<p class="info-form">отметь по пунктам те из них, которыми по праву гордишься<br>
(чтобы добавить поле нажимай + )
</p>
							<?if($arResult['PROPERTIES']['DOST']['VALUE']){?>
							<?foreach($arResult['PROPERTIES']['DOST']['VALUE'] as $key=>$itm){ $qwe=$key+1;?>
								<label class="label-form label-form-first" ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="DOST<?=$qwe?>" class="input-form input-form_first dost-<?=$qwe?> border" value="<?=$itm?>"></label>
							<?}
							}else{?><label class="label-form label-form-first" ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?>  name="DOST1" type="text" class="input-form input-form_first dost-1  border" value="<?=$itm?>"></label>
							<?}?>
     						<label class="label-form " ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="DOST<?=$qwe+1?>" class="input-form  dost-<?=$qwe+1?> border"><span class="span-label border">-</span></label>
     						<span class="span-float border">+</span>
     					</div>
						<br><p class="tittle-name_block ">фото достижений *</p>
						<p class="info-form">здесь прикрепи снимки, которые характеризуют самые значимые достижения  и обязательно подпиши, что и когда было, так будет интереснее читать твой профиль на сайте</p>
     					<div class="div-form dostfoto">
						<?if($arResult['FOTODOST']){
							foreach($arResult['FOTODOST'] as $key=> $el){?>
     						<div class="pswipe-block  border  ">
								<label class="label-form fileimg  <?=$el['clas']?> prewfot dostf-<?=$key?>" for="dostf-<?=$key?>">
								
										<?if(!$el['img']){?>  <p  id="txtf1"><br>Добавьте сюда<br> самую свою <br> удачную фотку</p><?}?>
										<input maxlength="250" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="dostf-<?=$key?>"  type="file" class="fileimg inputfile" id ="dostf-<?=$key?>"  onchange="rr(this);">			
								</label>
								
							</div>
							<br><label class="label-form " ><input maxlength="250" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="dostopis-<?=$key?>" id="dostopis-<?=$key?>" placeholder="описание фото" class="input-form input-form_first  border" value="<?=$el['opis']?>"></label>
								<br>
						<?}
						}else{?>
						    <div class="pswipe-block  border  ">
								<label class="label-form fileimg  <?=$el['clas']?> prewfot dostf-1" for="dostf-1">
								
										<?if(!$el['img']){?>  <p  id="txtf1"><br>Добавьте сюда<br> самую свою <br> удачную фотку</p><?}?>
										<input maxlength="250" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="dostf-1"  type="file" class="fileimg inputfile" id ="dostf-1"  onchange="rr(this);">			
								</label>
								
							</div>
							<br><label class="label-form " ><input maxlength="250" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="dostopis-1"  id="dostopis-1" placeholder="описание фото" class="input-form input-form_first  border" value=""></label>
								<br>
						<?}?>		
<span class="span-float plus border">+</span>
     					</div>
						<div class="div-form">
     						<p class="tittle-name_block">что для меня Ямал *</p>
     						
                                   <textarea <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="YAMAL" class="input-form border"><?=$arResult['PROPERTIES']['YAMAL']['~VALUE']['TEXT']?> </textarea>
     					</div>
     					<div class="div-form div-form_label">
     						<p class="tittle-name_block">любимое место на Ямале *</p>
     						<label class="label-form label-form-first" ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="LUBMESTO" type="text" class="input-form input-form_first  border" value="<?=$arResult['PROPERTIES']['LUBMESTO']['~VALUE']?>"></label>
     					<!--	<label class="label-form " ><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" class="input-form  border"><span class="span-label border">-</span></label>
     						<span class="span-float border">+</span>-->
     					</div>
						<div class="center-form">
						<div class="div-form">
     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS1']['NAME']?></p>
                             <textarea <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOPROS1" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS1']['~VALUE']['TEXT']?> </textarea>
    
     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS2']['NAME']?></p>
                             <textarea <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOPROS2" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS2']['~VALUE']['TEXT']?> </textarea>

     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS3']['NAME']?></p>
                             <textarea <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOPROS3" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS3']['~VALUE']['TEXT']?> </textarea>
    
     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS4']['NAME']?></p>
                             <textarea <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOPROS4" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS4']['~VALUE']['TEXT']?> </textarea>

     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS5']['NAME']?></p>
                             <textarea <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOPROS5" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS5']['~VALUE']['TEXT']?> </textarea>

     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS6']['NAME']?></p>
                             <textarea <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOPROS6" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS6']['~VALUE']['TEXT']?> </textarea>
     					</div>
						</div>
						
						
						
     					<div class="div-form check-div ">
     						<p class="info-form">всю информацию мы сохраним, возвращайся, когда тебе удобно и продолжай работу</p>
<p class="info-form">если грузишь большие фотокарточки с телефона, нужно запастись терпением - чем больше фотография, тем дольше загрузка;)</p>
<p class="info-form">прежде чем отправить на модерацию, проверь, все ли сейчас так, как хотелось бы. </p>
     						<div  class="label-check pc">
     							<input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="SOGL" type="checkbox" class="check-input"  <?if($arResult['PROPERTIES']['SOGL']['VALUE_ENUM_ID']){?>value="<?=$arResult['PROPERTIES']['SOGL']['VALUE_ENUM_ID']?>"<?}?> id="nag" required>
                                        <label for="nag"></label>
     							<span class="info-form">
Нажимая на  кнопки, вы даете согласие на обработку персональных данных</span>
     						</div>
     					<div class="buttons-block pc">
									<?if($arResult['IBLOCK_SECTION_ID']==9 || $arResult['IBLOCK_SECTION_ID']==17){?>
									<div class="button-form_sohr off">данные отправлены на модерацию</div>
									<?}elseif($arResult['IBLOCK_SECTION_ID']==12){?>
									<div class="button-form_sohr off">размещен на сайте</div>
									<?}elseif($arResult['IBLOCK_SECTION_ID']==6 || $arResult['IBLOCK_SECTION_ID']==7 || $arResult['IBLOCK_SECTION_ID']==8){?>
															
									<button class="button-form_sohr border clk_save ">СОХРАНИТЬ</button>
									<button  class="otpr moder">НА МОДЕРАЦИЮ</button>
									<?}?>
     						</div>
     					</div>
     				</div>
				
     			</div>
     			<div class="right-column border">
     			 <div class="right-column_block">
     					<h2 class="tittle-form">ПЕРСОНАЛЬНЫЕ ДАННЫЕ <span class="vopr border">?</span></h2>
     					<div  class="right-form">
     						<div class="div-form">
     							<p class="info-form info-form_margin">доступ к информации только у организатора проекта</p>
     							<label  class="label-form">
     								<span class="tittle-name_block uppercase"><?=$arResult['PROPERTIES']['EMAIL']['NAME']?> *</span>
     								<input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?>  name="EMAIL" type="text" class="input-form border" value="<?=$arResult['PROPERTIES']['EMAIL']['VALUE']?>">
     							</label>
     							<label  class="label-form">
     								<span class="tittle-name_block uppercase"><?=$arResult['PROPERTIES']['TELEFON']['NAME']?> *</span>
     								<input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="TELEFON" type="text" class="input-form border" placeholder="" value="<?=$arResult['PROPERTIES']['TELEFON']['VALUE']?>">
     							</label>
     							<label  class="label-form">
     								<span class="tittle-name_block uppercase"><?=$arResult['PROPERTIES']['MESTO']['NAME']?> *</span>
     								<input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="MESTO" type="text" class="input-form border" value="<?=$arResult['PROPERTIES']['MESTO']['VALUE']?>">
     							</label>								
     							<span class="tittle-name_block uppercase">скан документа *</span>
     							<div class="block-foto">
								
								<?$imgp=CFile::GetPath($arResult['PROPERTIES']['PASPORT']['VALUE']);?>							
							<label class="label-form_foto label-form  border  PASPORT fileimgpas PASPORTimg" for="PASPORT">
                                      <!-- <img id="DOCSVYAZ1" src="<?= $imgp?>" alt="">-->
                                      <span class="pl">1</span><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="PASPORT" type="file" id="PASPORT" class="fileimgpas inputfile" onchange="rr(this);" />
							</label>
						
						<?$imgd=CFile::GetPath($arResult['PROPERTIES']['DOCSVYAZ']['VALUE']);?>								
							<label class="label-form_foto label-form  border DOCSVYAZ fileimgpas DOCSVYAZimg" for="DOCSVYAZ">
                                      <!--  <img id="DOCSVYAZ1" src="<?= $imgd?>" alt="">-->
                                       
										<span class="pl">2</span><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="DOCSVYAZ" type='file' id="DOCSVYAZ" class="fileimgpas inputfile" onchange="rr(this);" />
							</label>
						
     								<!--<label class="label-form_foto label-form  border" ><span class="pl">+</span></label>-->
     							
     							</div>
     							<p class="info-form">1  —  скан либо фото документа, удостоверяющего личность, в котором указана дата твоего рождения</p>
     							<p class="info-form">2  —  документ подтверждающий твою связь с Ямалом (скан либо фото документа, удостоверяющего личность, в котором какой-либо населенный пункт Ямала указан в качестве места твоего рождения и/или проживания (прописка)</p>

     						</div>
     					</div>
     				</div>
     			</div>	
<div class="div-form check-div mob">
				<div  class="label-check ">
     							<input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="SOGL1" type="checkbox" class="check-input"  <?if($arResult['PROPERTIES']['SOGL']['VALUE_ENUM_ID']){?>value="<?=$arResult['PROPERTIES']['SOGL']['VALUE_ENUM_ID']?>"<?}?> id="nag1" required>
                                        <label for="nag1"></label>
     							<span class="info-form">
Нажимая на  кнопки, вы даете согласие на обработку персональных данных</span>
     						</div>
				                                        <div class="buttons-block buttons-block_mobail">

									<?if($arResult['IBLOCK_SECTION_ID']==9 || $arResult['IBLOCK_SECTION_ID']==17){?>
									<div class="button-form_sohr off">данные отправлены на модерацию</div>
									<?}elseif($arResult['IBLOCK_SECTION_ID']==12){?>
									<div class="button-form_sohr off">размещен на сайте</div>
									<?}elseif($arResult['IBLOCK_SECTION_ID']==6 || $arResult['IBLOCK_SECTION_ID']==7 || $arResult['IBLOCK_SECTION_ID']==8){?>
															
									<button class="button-form_sohr border clk_save ">СОХРАНИТЬ</button>
									<button  class="otpr moder">НА МОДЕРАЦИЮ</button>
									<?}?>
                                        </div>
										</div>
				
				</form>	

<?foreach($arResult['SVYAZ'] as $key => $sv){?>
	<?foreach($arResult['PROPERTIES']['SVYAZ']['VALUE_ENUM_ID'] as $checked){ 
		if($checked==$key){?>
		<script> 

		$('#givu<?=$key?>').click();</script>
	<?}
	}
	}?>	
<?foreach($arResult['CATEGORY'] as $key => $sv){
		if($arResult['PROPERTIES']['CATEGORY']['VALUE_ENUM_ID']==$key){?>
		<script type="text/javascript">
		var x=<?=$key?>;
		document.getElementById('categor').value= x;
		</script>
	<?}
	}?>		
<?if($arResult['PROPERTIES']['SOGL']['VALUE_ENUM_ID']){?>
	<script> $('#nag').click();$('#nag1').click();</script>
<?}?> 

<div id="result_form_uch"></div>
<style>
#result_form_uch{
background: #7700ff;
    position: fixed;
    z-index: 9999;
    min-width: 420px;
    color: white;
    right: 100%;
padding: 33% 0px;
    padding-left: 40px;
    padding-top: 15em;
    height: 60em;
    top: 0;
}
#result_form_uch .tittle-name_block{font-size: 18px;}
.dwiz {
    transform: translateX(100%);
    transition: all 1s cubic-bezier(0.9, 0, 1, 1);
}

</style>

	
<script>
function rr(elem){
	var idinp=elem.id;
	  if (elem.files[0]) {
	 // alert(idinp);
    var fr = new FileReader();
$("."+idinp).removeClass("prewf");
$("."+idinp).removeClass("PASPORTimg");
$("."+idinp).removeClass("DOCSVYAZimg");
<?foreach($arResult['FOTODOST'] as $key=>$el){$ke=$key+1;
	echo '$("."+idinp).removeClass("'.$el['clas'].'");';
}?>
if(idinp=="dost-1"){$("#txtf1").remove();}

    fr.addEventListener("load", function () {
		
      document.querySelector("."+idinp).style.backgroundImage = "url(" + fr.result + ")";
	  document.querySelector("."+idinp).style.backgroundSize = 'contain';
    }, false);

    fr.readAsDataURL(elem.files[0]);
  }
}
</script>


   