<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH ."/css/profile.css");

$this->setFrameMode(true);

$picture = CFile::ResizeImageGet($arResult['PREVIEW_PICTURE'], array('width' => 494, 'height' => 420), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true );
$pic=0;
?>

<script>
var fioDeti=<?php echo json_encode($arResult["PROPERTIES"]["FIODETI"]["NAME"]);?>;
var fioDetiVal=<?php echo json_encode($arResult["PROPERTIES"]["FIODETI"]["VALUE"]);?>;
var fioDetiCode=<?php echo json_encode($arResult["PROPERTIES"]["FIODETI"]["CODE"]);?>;
</script>

<?
if($arResult['PREVIEW_PICTURE']['SRC']){
?>
	<style>
	.prewf{background-image: url('<?= $picture["src"];?>');}
// 	.fd1 {background-image: url("<?= CFile::GetPath($arResult['PROPERTIES']['FOTODOST1']['VALUE']); ?>") !important; background-size: contain;}
// 	.fd2 {background-image: url("<?= CFile::GetPath($arResult['PROPERTIES']['FOTODOST2']['VALUE']); ?>") !important; background-size: contain;}
// 	.fd3 {background-image: url("<?= CFile::GetPath($arResult['PROPERTIES']['FOTODOST3']['VALUE']); ?>") !important; background-size: contain;}
// 	.fd4 {background-image: url("<?= CFile::GetPath($arResult['PROPERTIES']['FOTODOST4']['VALUE']); ?>") !important; background-size: contain;}
// 	.fd5 {background-image: url("<?= CFile::GetPath($arResult['PROPERTIES']['FOTODOST5']['VALUE']); ?>") !important; background-size: contain;}
	</style>
<?}?>


<?
if($arResult['PROPERTIES']['PASPORT']['VALUE']){
?>
	<?$imgp=CFile::GetPath($arResult['PROPERTIES']['PASPORT']['VALUE']);?>
	<style>
	.PASPORTimg{background-image: url('<?= $imgp;?>')!important;background-size: contain!important;}
	</style>
<?}?>

<?
if($arResult['PROPERTIES']['DOCSVYAZ']['VALUE']){?>
	<?$imgd=CFile::GetPath($arResult['PROPERTIES']['DOCSVYAZ']['VALUE']);?>
	<style>
	.DOCSVYAZimg{background-image: url('<?= $imgd;?>')!important;background-size: contain!important;}
	</style>
<?}?>

<?
$filesIds = array();
foreach($arResult['PROPERTIES']['FOTODOST']['~VALUE'] as $el) {
	$filesIds[] = $el;
}?>

	<div class="center-column">
		<form runat="server" name="ticket-form" id="ajax_form_uchsnik" enctype="multipart/form-data" action="/ajax/ajax_form_uchsnik.php">
			<input type="hidden" name="ID" id="id" value="<?=$arResult['ID']?>">
			<input type="hidden" name="USER_ID" id="userId" value="<?=$USER->GetID()?>">
			<input type="hidden" name="IBLOCKID" value="<?=$arResult['IBLOCK_ID']?>">
			<input type="hidden" name="SECTIONID" value="<?=$arResult['IBLOCK_SECTION_ID']?>">
			<input  type="hidden" name="ZAY" value="<?=$arResult['PROPERTIES']['ZAY']['VALUE_ENUM_ID']?>">
			<input  type="hidden" name="countdoct" id="countinp" value="<?=count($arResult['FOTODOST']);?>">
			<input  type="hidden" name="dostfilesids" id="dostfilesids" value="<?=implode(',',$filesIds);?>">
			<input  type="hidden" id="form-name-input" name="USER_NAME" value="<?=$arResult['PROPERTIES']['USER_NAME']['VALUE']?>">
			<input  type="hidden" id="form-lastname-input" name="USER_LASTNAME" value="<?=$arResult['PROPERTIES']['USER_LASTNAME']['VALUE']?>">
			<input  type="hidden" id="form-surname-input" name="USER_SURNAME" value="<?=$arResult['PROPERTIES']['USER_SURNAME']['VALUE']?>">
			<input  type="hidden" id="form-preview-picture" name="PREVIEW_PICTURE_SRC" value="<?=$arResult['PREVIEW_PICTURE']['SRC']?>">

                <h2 class="tittle-form">ЛИЧНЫЙ ПРОФИЛЬ</h2>

                    <div class="fio-block">
                        <div class="pswipe-block">
                            <div id="prewf111" class="file-area prewf">
                                <input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="prewpict" value="<?= $picture["src"];?>" type="file" class="fileimg inputfile" id ="dost-<?=$pic?>" onchange="updatePhoto(this);">
								<?
								    if(!$arResult['PREVIEW_PICTURE']['SRC']) {
								        ?><label for="dost-1">Добавь фото</label><?
								    }
								?>
                            </div>
                        </div>
                        <div class="pswipe-block-right">
							<label class="label-form"><input id="form-fio-input" placeholder="Ф.И.О." <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="USER_FIO" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['USER_FIO']['VALUE'])?>" required></label>
							<?if(false):?>
                                <label class="label-form"><input placeholder="Фамилия" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="USER_LASTNAME" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['USER_LASTNAME']['VALUE'])?>" required></label>
                                <label class="label-form"><input placeholder="Имя" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="USER_NAME" class="input-form input-form_first  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['USER_NAME']['VALUE'])?>" required></label>
                                <label class="label-form"><input placeholder="Отчество" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="USER_SURNAME" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['USER_SURNAME']['VALUE'])?>" required></label>
							<?endif?>
                            <div class="birth-date form-line">
                                <label class="label-form">
                                    <input id="birth-date" placeholder="Дата рождения" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="BIRTH_DATE" type="text" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['BIRTH_DATE']['VALUE'])?>"  onclick="BX.calendar({node: this, field: this, bTime: false});" required>
                                </label>
                                <div>
                                    <p>Полных лет на момент проведения отбора</p>
                                    <label class="label-form"><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOZRAST" type="text" class="input-form  border" value="<?=$arResult['PROPERTIES']['VOZRAST']['VALUE']?>" required></label>
                                </div>
                            </div>

                            <div class="form-line check-div">
                                <?foreach($arResult['SVYAZ'] as $key => $sv){?>
                                    <div class="label-form">
                                        <input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="radio" name="SVYAZ" class="check-input chek_sv" value="<?=$key?>" id="givu<?=$key?>"  />
                                        <label for="givu<?=$key?>"><?=$sv?></label>
                                    </div>
                                <?}?>
                            </div>

                            <label class="label-form label-form_age"><input placeholder="Место проживания на момент конкурса" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?>  name="GDEGIVESH" type="text" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['GDEGIVESH']['VALUE'])?>" required></label>

                            <div class="form-line">
                                <span>Размер одежды</span>
                                <label class="label-form label-form_age"><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?>  name="CLOTH_SIZE" type="text" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['CLOTH_SIZE']['VALUE'])?>" required size="4"></label>
                            </div>

                            <div class="div-form_select">
                                <h3 class="tittle-name">Выбери категорию участия</h3>
                                <select class="select-form cstego" name="CATEGORY_NEW" id="categor">
                                    <?
                                        $CATEGORY_NEW = array(
                                            41 => 'Хочу в ТОП-89: новые кандидаты в ТОП',
                                            42 => 'Знаю ТОЛК: кандидаты прошлых лет, желающие стать частью проекта ТОЛК',
                                            43 => 'ТОП-дети: от 7 до 17 лет'
                                        );
                                        foreach($CATEGORY_NEW as $key=>$el){
											?><option value="<?=$key?>" hidden><?=explode(':', $el)[0]?></option><?
											?><option value="<?=$key?>"><?=$el?></option><?
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-block div-form_select">
                        <h3 class="tittle-name">Мои контактные данные</h3>
                        <div class="form-line grow">
                            <label class="label-form "><input placeholder="Email" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="EMAIL" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['EMAIL']['VALUE'])?>" required></label>
                            <label class="label-form "><input placeholder="Номер телефона" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="TELEFON" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['TELEFON']['VALUE'])?>" required></label>
                        </div>
                        <p class="tittle-name">Основное место работы или учёбы</p>
                        <label class="label-form "><textarea placeholder="Укажи полное наименование организации и должность/специальность и курс." type="text" name="MESTO" class="input-form" required><?=htmlspecialchars_decode($arResult['PROPERTIES']['MESTO']['VALUE'])?></textarea></label>
                        <p class="tittle-name">Ссылки на соцсети, аккаунты должны быть открыты</p>
                        <div class="form-line grow">
                            <label class="label-form"><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VK" type="text" class="input-form  border" placeholder="ВКонтакте" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['VK']['VALUE'])?>"></label>
                            <label class="label-form"><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="OK" type="text" class="input-form  border" placeholder="Одноклассники" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['OK']['VALUE'])?>"></label>
                            <label class="label-form"><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="TELEGRAM" type="text" class="input-form  border" placeholder="Телеграм" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['TELEGRAM']['VALUE'])?>"></label>
                        </div>
                    </div>

                    <div id="top-children-parents-id" class="form-block top-children-parents<?=$arResult["PROPERTIES"]["CATEGORY_NEW"]["VALUE_ENUM_ID"] == "43" ? "" : "-hidden"?>">
                        <h3 class="tittle-name">Контактные данные родителя или законного представителя ребёнка</h3>
                        <div class="form-line grow">
                            <label class="label-form parent-data-fio"><input placeholder="ФИО" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="FIODETI" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['FIODETI']['VALUE'])?>"></label>
                            <label class="label-form parent-data-birthdate"><input placeholder="Дата рождения" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="PARENT_BIRTHDATE" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['PARENT_BIRTHDATE']['VALUE'])?>"></label>
                        </div>
                        <div class="form-line grow">
                            <label class="label-form parent-data-email"><input placeholder="Email" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="PARENT_EMAIL" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['PARENT_EMAIL']['VALUE'])?>"></label>
                            <label class="label-form parent-data-phone"><input placeholder="Номер телефона" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> type="text" name="TELEFONDETI" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['TELEFONDETI']['VALUE'])?>"></label>
                        </div>
                        <p class="tittle-name">Основное место работы</p>
                        <label class="label-form "><textarea placeholder="Укажи полное наименование организации и должность" type="text" name="PARENT_WORKPLACE" class="input-form" required><?=htmlspecialchars_decode($arResult['PROPERTIES']['PARENT_WORKPLACE']['VALUE'])?></textarea></label>
                        <p class="tittle-name">Ссылки на соцсети, аккаунты должны быть открыты</p>
                        <div class="form-line grow">
                            <label class="label-form"><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="PARENT_VK" type="text" class="input-form  border" placeholder="ВКонтакте" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['PARENT_VK']['VALUE'])?>"></label>
                            <label class="label-form"><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="PARENT_OK" type="text" class="input-form  border" placeholder="Одноклассники" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['PARENT_OK']['VALUE'])?>"></label>
                            <label class="label-form"><input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="PARENT_TELEGRAM" type="text" class="input-form  border" placeholder="Телеграм" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['PARENT_TELEGRAM']['VALUE'])?>"></label>
                        </div>
                    </div>

                    <div class="form-block">
                        <h3 class="tittle-name">Моя история</h3>
						<div id="top-winner-year-id" class="top-winner-year<?=$arResult["PROPERTIES"]["CATEGORY_NEW"]["VALUE_ENUM_ID"] == "42" ? "" : "-hidden"?>">
                            <p class="tittle-name">Финалист какого сезона ТОП-89</p>
                            <textarea placeholder="Напиши, в каком году и в каком городе ты участвовал в ТОП-89" maxlength="500"  <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="FINAL_YEAR" class="input-form border"><?=$arResult['PROPERTIES']['FINAL_YEAR']['VALUE']?></textarea>
                        </div>
                        <div>
                            <p class="tittle-name">Фокус деятельности и достижения</p>
                            <textarea placeholder="Расскажи о своей основной деятельности и достижениях за 2023 и 2024 годы"  <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="FOKUS" class="input-form border"><?=$arResult['PROPERTIES']['FOKUS']['~VALUE']['TEXT']?></textarea>
                        </div>
                        <h3 class="title-section">Документы, подтверждающие достижения за 2023 и 2024 годы.</h3>
                        <div>
							<p class="tittle-name">Заявки с подтверждением достижений будут иметь приоритет. Загрузи все файлы разом или прикрепи архив.</p>
                            <div class="files-wrapper">
                                <label class="files">
                                    Выбрать файлы
                                    <input placeholder="Прикрепить файлы" type="file" id="files" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="FOTODOST[]" value="<?= $arResult["PROPERTIES"]["FOTODOST"]["VALUE"] ?>" multiple>
                                </label>
                                <div class="files-icons" id="files-icons0">
                                    <?
                                        foreach($arResult["PROPERTIES"]["FOTODOST"]["VALUE"] as $fileId) {
                                            $fileDost = CFile::GetFileArray($fileId);
                                            ?>
                                                <div class="files-icon">
                                                    <img width="24" src="/upload/medialibrary/5b0/k5lq7mk2qbggk6kupp39hh67qz7m3x2y.png" height="24">
                                                    <p class="files-icon-title"><?= $fileDost["ORIGINAL_NAME"] ?></p>
                                                </div>
                                            <?
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="div-form">
                            <p class="tittle-name_block">Почему я достоин быть в топе?</p>
                            <textarea placeholder="Расскажи, почему именно ты заслуживаешь место в ТОП-89?" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOPR" class="input-form border"><?=htmlspecialchars_decode($arResult['PROPERTIES']['VOPR']['VALUE'])?></textarea>
                        </div>
                        <div class="div-form">
                            <p class="tittle-name_block">Что для меня Ямал?</p>
                            <textarea placeholder="Расскажи, что значит для тебя Ямал." <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="YAMAL" class="input-form border"><?=$arResult['PROPERTIES']['YAMAL']['~VALUE']['TEXT']?></textarea>
                        </div>
                        <div class="div-form div-form_label">
                            <p class="tittle-name_block">Моё любимое место на Ямале?</p>
                            <textarea placeholder="Расскажи о своём любимом месте на Ямале." <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="LUBMESTO" class="input-form border"><?=htmlspecialchars_decode($arResult['PROPERTIES']['LUBMESTO']['VALUE'])?></textarea>
                        </div>
                        <div class="div-form div-form_label">
                            <p class="tittle-name_block">Цитата, отражающая моё мировоззрение</p>
                            <textarea placeholder="Напиши цитату или фразу, которая тебя вдохновляет. Она может быть собственного сочинения." <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOPROS3" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS3']['~VALUE']['TEXT']?></textarea>
                        </div>
                        <div class="div-form div-form_label">
                            <p class="tittle-name_block">Мой опыт публичных выступлений</p>
                            <textarea placeholder="Расскажи о том, в каких форматах ты выступал публично.." <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOPROS7" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS7']['~VALUE']['TEXT']?></textarea>
                        </div>
                        <div class="div-form div-form_label">
                            <p class="tittle-name_block">О чем бы я рассказал аудитории из 150 человек?</p>
                            <textarea placeholder="Опиши в общих словах тему и ход своего выступления." <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="VOPROS8" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS8']['~VALUE']['TEXT']?></textarea>
                        </div>
                    </div>

                    <div class="form-block">
                        <h3 class="tittle-name">Видеовизитка </h3>
                        <div class="div-form_label">
                            <p class="tittle-name">Запиши видеовизитку, в которой отвечаешь на вопрос, почему ты должен стать частью ТОП-89. Это может быть рассказ про тебя и твои достижения. Смотри наши <a href="https://vk.com/@-199712030-instrukciya-po-videovizitke" target="_blank">рекомендации по записи</a>.</p>
                            <p class="tittle-name">Загрузи видеовизитку в соцсети, YouTube-канал или облачное хранилище. Обязательно открой доступ по ссылке для просмотра.</p>
                            <label class="label-form label-form_age"><input placeholder="Ссылка на видеовизитку/клип" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?>  name="VIDEO_CARD" type="text" class="input-form  border" value="<?=htmlspecialchars_decode($arResult['PROPERTIES']['VIDEO_CARD']['VALUE'])?>" required></label>
                        </div>

                        <?
                            for($i = 1; $i <= 5; $i++) {
                                ?>
                                    <div class="div-form_label" id="fotodost-<?=$i?>" <? if($i > 1 && strlen($arResult['PROPERTIES']['FACTDOST'.$i]['~VALUE']['TEXT']) == 0) echo "style=\"display:none\""; ?>>
										<textarea placeholder="<?if($i == 1) {echo 'Поделись интересными фактами о себе или другой важной информацией. Можешь прикрепить фотографии для наглядности.';} else { echo 'Расскажи о себе ещё';}?>" <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="FACTDOST<?=$i?>" class="input-form border"><?=$arResult['PROPERTIES']['FACTDOST'.$i]['~VALUE']['TEXT']?></textarea>
                                        <div class="files-wrapper">
                                            <label class="label-form files" for="fotodost<?=$i?>">
                                                Выбрать файлы
                                                <input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="FOTODOST<?=$i?>"  type="file" class="fileimg inputfile" id ="fotodost<?=$i?>" >
                                            </label>
                                            <div class="files-icons" id="files-icons<?=$i?>">
                                            <?
                                                $fileId = $arResult["PROPERTIES"]["FOTODOST".$i]["VALUE"];
												$fileDost = CFile::GetFileArray($fileId);
												if($_REQUEST['debug']) {
												    ?><pre><? print_r($fileDost); ?></pre><?
												}
												?>
													<div class="files-icon">
														<img width="24" src="/upload/medialibrary/5b0/k5lq7mk2qbggk6kupp39hh67qz7m3x2y.png" height="24">
														<p id="_FOTODOST<?=$i?>"class="files-icon-title"><?= $fileDost["ORIGINAL_NAME"] ?></p>
													</div>
												<?
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                <?
                            }
                        ?>

                        <div class="div-form_label">
                            <button type="button" class="flat field-add-button plus">
                                <svg role="icon">
                                    <use xlink:href='<? echo SITE_TEMPLATE_PATH; ?>/icons/plus.svg#icon' href="<? echo SITE_TEMPLATE_PATH; ?>/icons/plus.svg#icon"></use>
                                </svg>
                                Добавить еще поле
                            </button>
                        </div>
                    </div>

                    <div class="form-block check-div">
                        <div class="label-form">
                            <input <?if($arResult['IBLOCK_SECTION_ID']!=7 && $arResult['IBLOCK_SECTION_ID']!=6 &&$arResult['IBLOCK_SECTION_ID']!=8){?>readonly<?}?> name="SOGL" type="checkbox" class="check-input"  <?if($arResult['PROPERTIES']['SOGL']['VALUE_ENUM_ID']){?>value="<?=$arResult['PROPERTIES']['SOGL']['VALUE_ENUM_ID']?> checked"<?}?> id="nag" required>
							<label for="nag">Соглашаюсь на обработку <a href="https://xn--89-dlcaps4dk1f.xn--p1ai/rules-competition/" target="_blank">Персональных данных</a></label>
                        </div>
                    </div>
			<?if($arResult['PROPERTIES']['STATUS']['VALUE_ENUM_ID'] != 46 && $arResult['PROPERTIES']['STATUS']['VALUE_ENUM_ID'] != 47):?>
				<div class="buttons-block">
					<div>
						<?if($arResult['IBLOCK_SECTION_ID']==9 || $arResult['IBLOCK_SECTION_ID']==17){?>
							<div class="button-form_sohr off">данные отправлены на модерацию</div>
						<?}elseif($arResult['IBLOCK_SECTION_ID']==12){?>
							<div class="button-form_sohr off">размещен на сайте</div>
						<?}elseif($arResult['IBLOCK_SECTION_ID']==6 || $arResult['IBLOCK_SECTION_ID']==7 || $arResult['IBLOCK_SECTION_ID']==8){?>
							<button id="saver_btn1" class="button-form_sohr clk_save">Сохранить анкету</button>
							<button id="saver_btn2" class="otpr moder secondary">Отправить заявку на модерацию</button>
						<?}?>
					</div>
				</div>
			<?endif?>
        </form>
    </div>

<?if($arResult['PROPERTIES']['SVYAZ']['VALUE_ENUM_ID'][0]):?>
<script type="text/javascript">
	const select = document.getElementsByName('SVYAZ');

	for (let i = 0; i < select.length; i++) {
		if (select[i].value == <?=$arResult['PROPERTIES']['SVYAZ']['VALUE_ENUM_ID'][0]?>) select[i].checked = true;
	}
</script>
<?endif?>
<?if($arResult['PROPERTIES']['CATEGORY_NEW']['VALUE_ENUM_ID']):?>
<script>
	//document.getElementsByName('SVYAZ')[<?=$key?>].checked = true;
	const select2 = document.getElementById('categor');
	select2.value = <?=$arResult['PROPERTIES']['CATEGORY_NEW']['VALUE_ENUM_ID']?>;

</script>
<?endif?>

<div id="result_form_uch"></div>

<style>
.top-winner-year-hidden {
	display: none;
}
.top-winner-year {
	display: block;
}
.top-children-parents-hidden {
	display: none;
}
.top-children-parents {
	display: block;
}
</style>

<script>
$(".field-add-button").on('click', function() {
	for($i = 1; $i <= 5; $i++) {
		if ($('#fotodost-' + $i).is(':hidden')) {
			$('#fotodost-' + $i).show();
			break;
		}
	}
	if (
		$('#fotodost-1').is(':visible') &&
		$('#fotodost-2').is(':visible') &&
		$('#fotodost-3').is(':visible') &&
		$('#fotodost-4').is(':visible') &&
		$('#fotodost-5').is(':visible')
	) {
		$(".field-add-button").hide();
	}
});

	if (
		$('#fotodost-1').is(':visible') &&
		$('#fotodost-2').is(':visible') &&
		$('#fotodost-3').is(':visible') &&
		$('#fotodost-4').is(':visible') &&
		$('#fotodost-5').is(':visible')
	) {
		$(".field-add-button").hide();
	}

document.getElementById('categor').addEventListener('change', e => {
	e.target.value = e.target.value; //Меняем текст выбранного option на сокращенный вариант

	if (e.target.value == 42 ) { // Толк доп. поля
		document.getElementById('top-winner-year-id').classList.remove("top-winner-year-hidden");
		document.getElementById('top-winner-year-id').classList.add("top-winner-year");
	} else {
		document.getElementById('top-winner-year-id').classList.remove("top-winner-year");
		document.getElementById('top-winner-year-id').classList.add("top-winner-year-hidden");
	}

	if (e.target.value == 43 ) { // Дети доп. поля
		document.getElementById('top-children-parents-id').classList.remove("top-children-parents-hidden");
		document.getElementById('top-children-parents-id').classList.add("top-children-parents-year");

	} else {
		document.getElementById('top-children-parents-id').classList.remove("top-children-parents");
		document.getElementById('top-children-parents-id').classList.add("top-children-parents-hidden");
	}
});

$("#form-fio-input").on('change keydown paste input', function(){
	var vals = $(this).val();
	vals = vals ? vals : '';
	var valsArr = vals.split(' ');
	$('#form-name-input').val((valsArr.length > 1) ? valsArr[1] : '');
	$('#form-lastname-input').val(valsArr[0]);
	$('#form-surname-input').val((valsArr.length > 2) ? valsArr[2] : '');
});

var changesMade = false;

// Задействуем каждый элемент формы!
document.querySelectorAll('input, textarea, select').forEach(function(input) {
    input.onchange = function() {
        // Изменение зафиксировано!
        changesMade = true;
    };
});

window.onbeforeunload = function() {
    // Сбережем дату! Подтвердите ваше намерение уйти.
    if (changesMade) {
        return 'Вы уверены, что хотите покинуть страницу без сохранения внесенных изменений?';
    }
};

document.getElementById('saver_btn1').onclick = function() {
    window.onbeforeunload = null;
};
document.getElementById('saver_btn2').onclick = function() {
    window.onbeforeunload = null;
};

for($i = 1; $i <= 5; $i++) {
	document.getElementsByName('FOTODOST' + $i)[0].onchange = function() {

		if (this.files[0]) {
			changesMade = true;
			document.getElementById('_' + this.name).innerText = this.files[0].name;
		}
	};
}
var iconsHtml = '';
document.getElementById('files').onchange = function() {
	console.log(this.files);
	if (this.files[0]) {
		changesMade = true;
		iconsHtml = '';
	}
	while (document.getElementById('files-icons0').lastElementChild) {
		document.getElementById('files-icons0').removeChild(document.getElementById('files-icons0').lastElementChild);
	}
	Array.prototype.forEach.call(this.files, function(file) {
		iconsHtml += '<div class="files-icon">' +
						'<img width="24" src="/upload/medialibrary/5b0/k5lq7mk2qbggk6kupp39hh67qz7m3x2y.png" height="24">' +
						`<p class="files-icon-title">${file.name}</p>` +
					'</div>';
	 });

	console.log(document.getElementById('files-icons0'));
	document.getElementById('files-icons0').innerHTML = iconsHtml;

};

document.getElementsByName('prewpict')[0].onchange = function() {
	if (this.files[0]) {
		changesMade = true;
		var target = event.target;

		if (!FileReader) {
			alert('FileReader не поддерживается');
			return;
		}

		if (!target.files.length) {
			return;
		}

		var fileReader = new FileReader();
		fileReader.onload = function() {
			console.log(`url("${fileReader.result}")`);

			document.getElementsByClassName('prewf')[0].style.backgroundImage = `url("${fileReader.result}")`;
			console.log(document.getElementsByClassName('prewf')[0].style);
		}

		fileReader.readAsDataURL(target.files[0]);
	}

}
</script>
