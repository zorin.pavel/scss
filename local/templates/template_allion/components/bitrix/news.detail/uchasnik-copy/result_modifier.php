<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"ASC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>1, "CODE"=>"SVYAZ"));
while ($enum_fields = $property_enums->GetNext()) {
	$arResult['SVYAZ'][$enum_fields["ID"]]=$enum_fields["VALUE"];
}
$property_enums = CIBlockPropertyEnum::GetList(Array("ID"=>"ASC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>1, "CODE"=>"CATEGORY"));
while ($enum_fields = $property_enums->GetNext()) {
	$arResult['CATEGORY'][$enum_fields["ID"]]=$enum_fields["VALUE"];
}
if($arResult["PROPERTIES"]["FOTODOST"]){
	foreach($arResult["PROPERTIES"]["FOTODOST"]["VALUE"] as $key=>$el){
		$FOTODOSTmass[$key]["clas"]='DOSTimg'.$key;
		$FOTODOSTmass[$key]["img"]=CFile::GetPath($el);
	$FOTODOSTmass[$key]["opis"]=$arResult["PROPERTIES"]["FOTODOST"]["DESCRIPTION"][$key];
	}
}
$arResult['FOTODOST']=$FOTODOSTmass;
?>
