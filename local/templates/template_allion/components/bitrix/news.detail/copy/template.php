<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$fio = explode(" ", $arResult['NAME']);
$picture = CFile::ResizeImageGet($arResult['PREVIEW_PICTURE'], array('width' => 494, 'height' => 420), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true );
$pic=0;
?>

<?
if($arResult['PREVIEW_PICTURE']['SRC']){?>
<style>
.prewf{background-image: url('<?= $picture["src"];?>')!important;}
</style>
<?}?>	


<div class="center-column">

				<form runat="server" id="ajax_form_uchsnik" enctype="multipart/form-data" action="/ajax/ajax_form_uchsnik.php">
				<input type="hidden" name="ID" value="<?=$arResult['ID']?>">
				<input type="hidden" name="IBLOCKID" value="<?=$arResult['IBLOCK_ID']?>">
				<input type="hidden" name="SECTIONID" value="<?=$arResult['IBLOCK_SECTION_ID']?>">
                    <div class="center-form center-form_first">
     					<h2 class="tittle-form">ЛИЧНЫЙ ПРОФИЛЬ</h2>
     					<div class="div-form">
     						<div class="pswipe-block border">
							<label class="label-form fileimg  prewf prewfot dost-<?echo $pic=$pic+1?>" for="dost-<?echo $pic?>">
							
									<?if(!$arResult['PREVIEW_PICTURE']['SRC']){?>  <p  id="txtf1"><br>Добавьте сюда<br> самую свою <br> удачную фотку</p><?}?>
									<input name="prewpict" value="<?= $picture["src"];?>" type="file" class="fileimg inputfile" id ="dost-<?=$pic?>" onchange="rr(this);">
									
								</label>
							
							
			
                                   </div>
     					</div>
     					<div class="div-form">
     						<p class="tittle-name_block">имя*</p>
     						<label class="label-form " ><input type="text" name="name" class="input-form input-form_first  border" value="<?=$fio[0]?>"></label>
     						<p class="tittle-name_block">фамилия*</p>
     						<label class="label-form " ><input type="text" name="fam" class="input-form  border" value="<?=$fio[1]?>"></label>	
     						<p class="tittle-name_block">возраст*</p>
     						<label class="label-form label-form_age" ><input name="VOZRAST" type="text" class="input-form  border" value="<?=$arResult['PROPERTIES']['VOZRAST']['VALUE']?>" required></label>
     					</div>
     					<div class="div-form check-div">
						<?foreach($arResult['SVYAZ'] as $key => $sv){?>
     						<div class="label-form " >
     							<input type="radio" name="SVYAZ<?=$key?>" class="check-input chek_sv" value="<?=$key?>" id="givu<?=$key?>" onclick="clk_sv('givu<?=$key?>');" >	
								<label for="givu<?=$key?>"></label>
     							<span class="tittle-name_block-span"><?=$sv?></span>
     						</div>  						
						<?}?>	
     					</div>
						<div class="div-form">
						    <p class="tittle-name_block">где ты живешь сейчас*</p>
     						<label class="label-form label-form_age" ><input  name="GDEGIVESH" type="text" class="input-form  border" value="<?=$arResult['PROPERTIES']['GDEGIVESH']['VALUE']?>" required></label>
						</div>
                         <div class="div-form div-form_select">
                         	<p class="tittle-name_block">категория рейтинга:*</p>
                         	<select class="select-form border" name="CATEGORY" id="categor">
								<?foreach($arResult['CATEGORY'] as $key=>$el){?>
									<option value="<?=$key?>"><?=$el?></option>
								<?}?>
                         	</select>
                         	<p class="tittle-name_block tittle-name_block-margin">соцсети:</p>
                         	<label class="label-form" ><input name="VK" type="text" class="input-form  border" placeholder="ВК" value="<?=$arResult['PROPERTIES']['VK']['VALUE']?>"></label>
                         	<label class="label-form" ><input name="FACEBOOK" type="text" class="input-form  border" placeholder="FB" value="<?=$arResult['PROPERTIES']['FACEBOOK']['VALUE']?>"></label>
                         	<label class="label-form" ><input name="INST" type="text" class="input-form  border" placeholder="Instagram" value="<?=$arResult['PROPERTIES']['INST']['VALUE']?>"></label>
                         	<label class="label-form" ><input name="TIKTOK" type="text" class="input-form  border" placeholder="ТikTok" value="<?=$arResult['PROPERTIES']['TIKTOK']['VALUE']?>"></label>
                         	<label class="label-form" ><input name="YOUTUBE" type="text" class="input-form  border" placeholder="YouTube" value="<?=$arResult['PROPERTIES']['YOUTUBE']['VALUE']?>"></label>
                         </div>
     				</div>
     				<div class="center-form">
     					<h2 class="tittle-form">ДОСЬЕ</h2>
     					<div class="div-form">
     						<p class="tittle-name_block">фокус деятельности*<span class="vopr border">?</span></p>
     						<p class="info-form">Коротко о том, на что вы делаете акцент в своей работе, что считаете целью своей деятельности, на какой результат ориентируетесь</p>
                                   <textarea name="FOKUS" class="input-form border"><?=$arResult['PROPERTIES']['FOKUS']['~VALUE']['TEXT']?> </textarea>
     					</div>
     					<div class="div-form div-form_label">
     						<p class="tittle-name_block">достижения 2021 года*</p>
     						<p class="info-form">Отметьте по пунктам те из них, которыми по праву гордитесь сами — отдельные поля для заполнения 8 достижений максимум.</p>
							<?if($arResult['PROPERTIES']['DOST']['VALUE']){?>
							<?foreach($arResult['PROPERTIES']['DOST']['VALUE'] as $itm){?>
								<label class="label-form label-form-first" ><input type="text" name="DOST" class="input-form input-form_first  border" value="<?=$itm?>"></label>
							<?}
							}else{?><label class="label-form label-form-first" ><input  name="DOST" type="text" class="input-form input-form_first  border" value="<?=$itm?>"></label><?}?>
     						<label class="label-form " ><input type="text" class="input-form  border"><span class="span-label border">-</span></label>
     						<span class="span-float border">+</span>
     					</div>
     					<div class="div-form_label div-form div-form_foto">
     						<p class="tittle-name_block">фото достижений*</p>
     						<div class="block-foto">
							<?foreach($arResult['PROPERTIES']['FOTODOST']['VALUE'] as $key =>$itm){
								$img=CFile::GetPath($itm);?>
								
							<label class="label-form_foto label-form  border" for="imgInp">
                                        <img id="blah" src="<?= $img?>" alt="">
                                       
										<?if(!$img){?><input  name="FOTODOST<?=$key?>" type='file' id="imgInp" /><?}?>
							</label>
							<?}?>	
     							<label class="label-form_foto label-form foto-first border fileimg dost-<?echo $pic=$pic+1?>" for="dost-<?=$pic?>" >
									<span class="span-label border">-</span>
									<input type="file" name="FOTODOST<?=$key?>" class="fileimg inputfile" id ="dost-<?=$pic?>" onchange="rr(this);"> 
								</label>
								
								
     							<span class="span-float_foto border">+</span>
     						</div>
     					</div>
						<div class="div-form">
     						<p class="tittle-name_block">что для меня ЯМАЛ*</p>
     						<p class="info-form">Коротко о том, на что вы делаете акцент в своей работе, что считаете целью своей деятельности, на какой результат ориентируетесь</p>
                                   <textarea name="YAMAL" class="input-form border"><?=$arResult['PROPERTIES']['YAMAL']['~VALUE']['TEXT']?> </textarea>
     					</div>
     					<div class="div-form div-form_label">
     						<p class="tittle-name_block">любимое место на Ямале*</p>
     						<label class="label-form label-form-first" ><input name="LUBMESTO" type="text" class="input-form input-form_first  border" value="<?=$arResult['PROPERTIES']['LUBMESTO']['~VALUE']?>"></label>
     					<!--	<label class="label-form " ><input type="text" class="input-form  border"><span class="span-label border">-</span></label>
     						<span class="span-float border">+</span>-->
     					</div>
						<div class="center-form">
						<div class="div-form">
     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS1']['NAME']?></p>
                             <textarea name="VOPROS1" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS1']['~VALUE']['TEXT']?> </textarea>
    
     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS2']['NAME']?></p>
                             <textarea name="VOPROS2" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS2']['~VALUE']['TEXT']?> </textarea>
 
     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS3']['NAME']?></p>
                             <textarea name="VOPROS3" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS3']['~VALUE']['TEXT']?> </textarea>
    
     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS4']['NAME']?></p>
                             <textarea name="VOPROS4" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS4']['~VALUE']['TEXT']?> </textarea>

     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS5']['NAME']?></p>
                             <textarea name="VOPROS5" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS5']['~VALUE']['TEXT']?> </textarea>

     						<p class="tittle-name_block"><?=$arResult['PROPERTIES']['VOPROS6']['NAME']?></p>
                             <textarea name="VOPROS6" class="input-form border"><?=$arResult['PROPERTIES']['VOPROS6']['~VALUE']['TEXT']?> </textarea>
     					</div>
						</div>
     					<div class="div-form check-div ">
     						<p class="info-form">Мы позаботились, чтобы тебе было максимально удобно работать в кабинете, ты можешь сохранить работу по оформлению своего профиля на любом этапе и вернуться когда это будет тебе удобно. </p>
     						<p class="info-form">Как только ты будешь уверен на 100% что все готово, отправлен на модерацию, после этого редактировать ты уже не сможешь, а мы будем знакомиться с тобой и проверим всё, чтобы разместить твой профиль на сайте.</p>
     						<div  class="label-check pc">
     							<input name="SOGL" type="checkbox" class="check-input"  <?if($arResult['PROPERTIES']['SOGL']['VALUE_ENUM_ID']){?>value="<?=$arResult['PROPERTIES']['SOGL']['VALUE_ENUM_ID']?>"<?}?> id="nag" required>
                                        <label for="nag"></label>
     							<span class="info-form">Нажимая на  кнопки, вы даете согласие на обработку персональных данных</span>
     						</div>
     					<div class="buttons-block pc">
     							<button class="button-form_sohr border clk_save ">СОХРАНИТЬ</button>
     						<!--	<button type="submit" class="otpr">На модерацию</button>-->
     						</div>
     					</div>
     				</div>
				
     			</div>
     			<div class="right-column border">
     			 <div class="right-column_block">
     					<h2 class="tittle-form">ПЕРСОНАЛЬНЫЕ ДАННЫЕ <span class="vopr border">?</span></h2>
     					<div  class="right-form">
     						<div class="div-form">
     							<p class="info-form info-form_margin">Доступ к информации только у админов проекта</p>
     							<label  class="label-form">
     								<span class="tittle-name_block uppercase"><?=$arResult['PROPERTIES']['NAME']?></span>
     								<input  name="EMAIL" type="text" class="input-form border" value="<?=$arResult['PROPERTIES']['EMAIL']['VALUE']?>">
     							</label>
     							<label  class="label-form">
     								<span class="tittle-name_block uppercase"><?=$arResult['PROPERTIES']['TELEFON']['NAME']?></span>
     								<input name="TELEFON" type="text" class="input-form border" placeholder="" value="<?=$arResult['PROPERTIES']['TELEFON']['VALUE']?>">
     							</label>
     							<label  class="label-form">
     								<span class="tittle-name_block uppercase"><?=$arResult['PROPERTIES']['MESTO']['NAME']?></span>
     								<input name="MESTO" type="text" class="input-form border" value="<?=$arResult['PROPERTIES']['MESTO']['VALUE']?>">
     							</label>								
     							<span class="tittle-name_block uppercase">скан документа</span>
     							<div class="block-foto">
								
								<?$imgw=CFile::GetPath($arResult['PROPERTIES']['PASPORT']['VALUE']);?>							
							<label class="label-form_foto label-form  border  dost-<?echo $pic=$pic+1?> fileimgpas" for="dost-<?echo $pic?>">
                                        <img id="blah" src="<?= $imgw?>" alt="">
                                       
										<?if(!$img){?><span class="pl">+</span><input name="PASPORT" type='file' id="dost-<?echo $pic?>" class="fileimgpas inputfile" onchange="rr(this);" /><?}?>
							</label>
						
						<?$imgw=CFile::GetPath($arResult['PROPERTIES']['DOCSVYAZ']['VALUE']);?>								
							<label class="label-form_foto label-form  border dost-<?echo $pic=$pic+1?> fileimgpas" for="dost-<?echo $pic?>">
                                        <img id="blah" src="<?= $imgw?>" alt="">
                                       
										<?if(!$img){?><span class="pl">+</span><input name="DOCSVYAZ" type='file' id="dost-<?echo $pic?>" class="fileimgpas inputfile" onchange="rr(this);" /><?}?>
							</label>
						
     								<!--<label class="label-form_foto label-form  border" ><span class="pl">+</span></label>-->
     							
     							</div>
     							<p class="info-form">Фотография паспорта (обязательно) прикрепите скан либо фото документа, удостоверяющего личность, в котором указана дата вашего рождения.</p>
     							<p class="info-form">Документ подтверждающий вашу связь с Ямалом (прикрепите скан либо фото документа, удостоверяющего личность, в котором какой-либо населенный пункт Ямала указан в качестве места вашего рождения и/или проживания (прописка) Место учебы, работы (если учитесь и работаете одновременно, укажите то место, деятельность в котором для вас в приоритете)</p>
                                        <div class="buttons-block buttons-block_mobail">
										
                                             <button class="button-form_sohr border clk_save">СОХРАНИТЬ</button>
                                            <!-- <button type="submit" class="otpr">На модерацию</button>-->
                                        </div>
     						</div>
     					</div>
     				</div>
     			</div>
				</form>	

<?foreach($arResult['SVYAZ'] as $key => $sv){?>
	<?foreach($arResult['PROPERTIES']['SVYAZ']['VALUE_ENUM_ID'] as $checked){ 
		if($checked==$key){?>
		<script> 

		$('#givu<?=$key?>').click();</script>
	<?}
	}
	}?>	
<?foreach($arResult['CATEGORY'] as $key => $sv){
		if($arResult['PROPERTIES']['CATEGORY']['VALUE_ENUM_ID']==$key){?>
		<script type="text/javascript">
		var x=<?=$key?>;
		document.getElementById('categor').value= x;
		</script>
	<?}
	}?>		
<?if($arResult['PROPERTIES']['SOGL']['VALUE_ENUM_ID']){?>
	<script> $('#nag').click();</script>
<?}?> 

<div id="result_form_uch"></div>
<style>
#result_form_uch{
background: #7700ff;
    position: fixed;
    z-index: 9999;
    min-width: 420px;
    color: white;
    right: 100%;
padding: 33% 0px;
    padding-left: 40px;
    padding-top: 15em;
    height: 60em;
    top: 0;
}
#result_form_uch .tittle-name_block{font-size: 18px;}
.dwiz {
    transform: translateX(100%);
    transition: all 1s cubic-bezier(0.9, 0, 1, 1);
}

</style>

	
