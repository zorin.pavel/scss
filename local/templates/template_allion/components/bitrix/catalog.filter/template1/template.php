<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$isShow = true;
?>
<?if($arParams["ONLY_ON_VOTING"] == 'Y') {
	if (!isContestTime()) {
		$isShow = false;
	}
}?>

<?if($isShow):?>
    <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="filters">
        <?
            foreach($arResult["ITEMS"] as $arItem) {
                if(array_key_exists("HIDDEN", $arItem)) {
                    echo $arItem["INPUT"];
                }
            }
        ?>
        <div class="category-buttons">
			<?if(!$arParams["SHOW_CATEGORIES"] == "N"):?>
            <ul class="category-buttons-list">
                <li>
                    <input type="radio" name="category" id="all-category" value="all" />
                    <label for="all-category">Все</label>
                </li>
                <li>
                    <input type="radio" name="category" id="tolk-category" value="tolk" />
                    <label for="tolk-category">Знают толк</label>
                </li>
                <li>
                    <input type="radio" name="category" id="top-children-category" value="top-children" />
                    <label for="top-children-category">ТОП-дети</label>
                </li>
            </ul>
            <script>
                var activeCategory = '<?=$_GET['category']?>';
                if (!!activeCategory) {
                    var radio = $(`#${activeCategory}-category`);
                    if (radio.is(':checked') === false) {
                        radio.prop('checked', true);
                    }
                }
            </script>
			<?endif?>
        </div>

        <div class="search-string">
            <input type="text" name="arrFilter_ff[NAME]" class="search" value="<?if(!empty($_GET['arrFilter_ff']['NAME'])){?><?= $_GET['arrFilter_ff']['NAME']?><?}?>">
            <svg role="icon button" class="search-button-filter">
                <use xlink:href='<?=SITE_TEMPLATE_PATH?>/icons/search.svg#icon' href="<?=SITE_TEMPLATE_PATH?>/icons/search.svg#icon"></use>
            </svg>
            <?
                if(!empty($_GET['arrFilter_ff']['NAME']) || !empty($_GET['category'])){
                    ?>
                        <label>
                            <input type="submit" name="del_filter" class="flat del-button-filter" value="Очистить" />
                        </label>
                    <?
                }
            ?>
        </div>

        <script>
			const pageAddress = './'; // <?=$arParams["PAGE_ADDRESS"]?>' ?? './';

            $('.search-button-filter').on('click', function(e) {
                e.preventDefault();
                var categoryName = $('input[name="category"]:checked').val();
                var categoryQuery = categoryName ? `category=${categoryName}` : '';
                var searchVal = $('input[name="arrFilter_ff[NAME]"]').val();
                var searchValQuery = searchVal ? `arrFilter_ff[NAME]=${searchVal}` : '';

				// фильтры победителей, которых не судят
				var wonYears = $('.winners-filter-item-year');
				var arrYears = wonYears.length > 0 ? wonYears.map( function( ) {
					if (this.checked) {
						return this.value;
					}
					return null;
				}).get() : [];
				var wonYearsQuery = arrYears.length > 0 ? 'winner_years=' + arrYears.join(',') : '';

				var wonCategories = $('.winners-filter-item-category1');
				var arrCategories = wonCategories.length > 0 ? wonCategories.map( function( ) {
					if (this.checked) {
						return this.value;
					}
					return null;
				}).get() : [];
				var arrCategoriesQuery = arrCategories.length > 0 ? 'winner_categories=' + arrCategories.join(',') : '';

                var queryString = [categoryQuery, searchValQuery, wonYearsQuery, arrCategoriesQuery, 'set_filter=Y'].filter(el => el.trim() !== '').join('&');
				document.location.href = `${pageAddress}?${queryString}`;
            })
            $('.del-button-filter').on('click', function(e) {
				console.log(`${pageAddress}?del_filter=Очистить`);
                e.preventDefault();
				document.location.href = `${pageAddress}?del_filter=Очистить`;
            });

			var yearsList = "<?=$_GET['winner_years']?>";
			var yearsListArr = yearsList.length > 0 ? yearsList.split(',') : [];
			$('.winners-filter-item-year').each(function() {
				if (yearsListArr.find((el) => el === $(this).val())) {
					$(this).prop('checked', true);
				}
			});

			var oldCategoriesList = "<?=$_GET['winner_categories']?>";
			var oldCategoriesListArr = oldCategoriesList.length > 0 ? oldCategoriesList.split(',') : [];
			$('[name="winner_categories"]').each(function() {
				if (oldCategoriesListArr.find((el) => el === $(this).val())) {
					$(this).prop('checked', true);
				}
			});

			$('input[name="winner-filter-year-all"]').on('click', function(e) {
				//if($('input[name="winner-filter-year-all"]')) {
					$('.winners-filter-item-year').each(function() {
						$(this).prop('checked', false);
					});
				// }
			});

			$('input[name="winner-filter-category-all"]').on('click', function(e) {
                $('[name="winner_categories"]').prop('checked', false);
			});

			$('[name="winners_filter_form"] :input').change((e) => {
				$('[name="winners_filter_form"]').submit();
			});
        </script>
    </form>
<?endif?>
