<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH ."/css/voting.css");

$this->setFrameMode(true);
$isShow = true;

?>

<?
$arGroupAvalaible = array('9'); // массив групп, которые в которых нужно проверить доступность пользователя
$arGroups = CUser::GetUserGroup($USER->GetID()); // массив групп, в которых состоит пользователь
$result_intersect = array_intersect($arGroupAvalaible, $arGroups);// далее проверяем, если пользователь вошёл хотя бы в одну из групп, то позволяем ему что-либо делать
?>


<?if($arParams["ONLY_ON_VOTING"] == 'Y') {
	if (!isContestTime()) {
		$isShow = false;
	}
}?>

<?if($isShow):?>
	<?if(isset($arParams["DISPLAY_CATEGORY_COUNT"]) && $arParams["DISPLAY_CATEGORY_COUNT"] == 'Y'):?>
	<div class="news-list-cat-count">
		<p>Количество участников: <?=count($arResult["ITEMS"])?></p><br />
	</div>
	<?endif;?>
	<div class="news-list">
	<?if($arParams["DISPLAY_TOP_PAGER"]):?>
		<?=$arResult["NAV_STRING"]?><br />
	<?endif;?>
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<div class="person-vote-card">
			<div class="person-vote-card-image-container">
				<div class="person-vote-card-image">
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
						<img class="person-vote-card-image lazy" src="<?=SITE_TEMPLATE_PATH?>/img/spacer.gif" data-src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" />
					</a>
				</div>
				<?if($arItem["PROPERTIES"]["LIGHTENED"]["VALUE"] > 0 || $arItem['PROPERTIES']['ZAY']['VALUE_ENUM_ID']==20 ):?>
					<div class="person-lightened">
						<div class="person-lightened-text-container">
							<p class="person-lightened-text">Меня высветили</p>
						</div>
						<svg role="icon button" class="person-lightened-img">
							<use xlink:href='<?=SITE_TEMPLATE_PATH?>/icons/sun.svg#icon' href="<?=SITE_TEMPLATE_PATH?>/icons/sun.svg#icon"></use>
						</svg>
					</div>
				<?endif?>
			</div>
			<div class="person-vote-card-name-container">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
					<p class="person-vote-name"><?echo $arItem["NAME"]?></p>
				</a>
			</div>
			<div class="person-vote-card-button-container">
				<a class="person-vote-card-button-container" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
					<button id="voting-button-details-id" type="button" class="person-vote-card-button-details">Подробнее</button>
				</a>
				<?if($arParams["SHOW_CONTROL_BUTTONS"] != "N"):?>
					<? if(empty($result_intersect) && isContestTime()): ?>
						<button id="voting-button-vote-id" type="button" class="person-vote-card-button-vote">Проголосовать</button>
					<?endif?>
					<form runat="server" name="moderator-form" id="ajax_form_moderator-<?=$arItem['ID']?>" enctype="multipart/form-data" action="/ajax/ajax_moderator_new.php">
						<input type="hidden" name="ID" id="id" value="<?=$arItem['ID']?>">
						<input type="hidden" name="USER_ID" id="userId" value="<?=$USER->GetID()?>">
						<input type="hidden" name="IBLOCKID" value="<?=$arItem['IBLOCK_ID']?>">
						<input type="hidden" name="SECTIONID" value="<?=$arItem['IBLOCK_SECTION_ID']?>">
						<div class="person-vote-card-button-container">
							<? if(!empty($result_intersect) && $arItem["PROPERTIES"]["STATUS"]["VALUE_ENUM_ID"] != "46"): ?><button id="moderator-accept" type="button" class="person-vote-card-button-accept" onclick="JavaScript:return Validator(this.id, <?=$arItem['ID']?>, 'Продвердите, что принимаете заявку!');">Принять</button><?endif?>
							<? if(!empty($result_intersect) && $arItem["PROPERTIES"]["STATUS"]["VALUE_ENUM_ID"] != "45"): ?><button id="moderator-toedit" type="button" class="person-vote-card-button-toedit" onclick="JavaScript:return Validator(this.id, <?=$arItem['ID']?>, 'Продвердите, что отправляете заявку на доработку!');">На доработку</button><?endif?>
							<? if(!empty($result_intersect) && $arItem["PROPERTIES"]["STATUS"]["VALUE_ENUM_ID"] != "48"): ?><button id="moderator-decline" type="button" class="person-vote-card-button-decline" onclick="JavaScript:return Validator(this.id, <?=$arItem['ID']?>, 'Продвердите, что отклоняете заявку!');">Отклонить</button><?endif?>

							<? if(!empty($result_intersect) && $arItem["PROPERTIES"]["LIGHTENED"]["VALUE"] != '1'): ?><button id="moderator-enlight" type="button" class="person-vote-card-button-enlight" onclick="JavaScript:return Validator(this.id, <?=$arItem['ID']?>, 'Продвердите, что высвечиваете заявку!');">Высветить</button><?endif?>
							<? if(!empty($result_intersect) && $arItem["PROPERTIES"]["LIGHTENED"]["VALUE"] == '1'): ?><button id="moderator-unenlight" type="button" class="person-vote-card-button-unenlite" onclick="JavaScript:return Validator(this.id, <?=$arItem['ID']?>, 'Продвердите, что снимаете высвечивание с заявки!');">Снять подсветку</button><?endif?>
						</div>
					</form>
				<?endif?>
			</div>
		</div>
		<?if(false):?>
		<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<p class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
				<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
						<img
							class="preview_picture"
							border="0"
							src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
							width="<?=intval(($arItem["PREVIEW_PICTURE"]["WIDTH"] ? $arItem["PREVIEW_PICTURE"]["WIDTH"] : 0) / 10) ?>"
							height="<?=intval(($arItem["PREVIEW_PICTURE"]["HEIGHT"] ? $arItem["PREVIEW_PICTURE"]["HEIGHT"] : 0) / 10)?>"
							alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
							title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
							style="float:left"
						/>
					</a>
				<?else:?>
					<img
						class="preview_picture"
						border="0"
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						style="float:left"
						/>
				<?endif;?>
			<?endif?>
			<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
				<span class="news-date-time"><?echo $arItem["DISPLAY_ACTIVE_FROM"]?></span>
			<?endif?>
			<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
				<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
					<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><b><?echo $arItem["NAME"]?></b></a><br />
				<?else:?>
					<b><?echo $arItem["NAME"]?></b><br />
				<?endif;?>
			<?endif;?>
			<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
				<?echo $arItem["PREVIEW_TEXT"];?>
			<?endif;?>

			<?foreach($arItem["FIELDS"] as $code=>$value):?>
				<small>
				<?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?>
				</small><br />
			<?endforeach;?>
			<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
				<small>
				<?=$arProperty["NAME"]?>:&nbsp;
				<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
					<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
				<?else:?>
					<?=$arProperty["DISPLAY_VALUE"];?>
				<?endif?>
				</small><br />
			<?endforeach;?>
		</p>
		<?endif?>
	<?endforeach;?>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>

</div>
<?endif;?>
<?if(!$isShow && $arParams["ONLY_ON_VOTING"] == 'Y'):?>
	<div class="news-list">
		<p style="font-size:24px;">Голосование еще не началось!</p>
	</div>
<?endif;?>
<div id="result_form_uch"></div>

<style>
#result_form_uch.dwizm {
	transform: translateX(0);
}

#result_form_uch {
  background: #C8937D;
  position: fixed;
  z-index: 9999;
  width: 22.5rem;
  left: 0;
  top: 0;
  bottom: 0;
  transform: translateX(-100%);
  transition: all 0.5s ease-in; }
  #result_form_uch .result_form-wrapper {
    position: absolute;
    bottom: 50%;
    padding: 1rem; }
  #result_form_uch .tittle-name_block {
    margin-bottom: 1.5rem; }
  #result_form_uch.dwiz {
    transform: translateX(0); }
</style>

<script>

function Validator(id_, ajaxId, text){
   if(confirm(text) ){
      sendModeratorAjax(id_, ajaxId);
      return(true);
   }else{
      return(false);
   }
}

function clos() {
    $('#result_form').removeClass('dwizm');
    $('#result_form_uch').removeClass('dwizm');
    location.reload();
};

function closModer() {
    $('#result_form').removeClass('dwizm');
    $('#result_form_uch').removeClass('dwizm');

};

function sendModeratorAjax(id, ajaxId) {
	var $this = $(`#ajax_form_moderator-${ajaxId}`);
	var $form = $this.closest('form');
	var formUrl = '/ajax/ajax_moderator_new.php';
	$("<input />").attr("type", "hidden")
          .attr("name", id)
          .attr("value", id)
          .appendTo($form);

	$.ajax({
		url: formUrl,
		method: 'post',
		type: 'POST',             // Type of request to be send, called as method
		data: new FormData($form.get(0)), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
		contentType: false,       // The content type used when sending data to the server.
		cache: false,             // To unable request pages to be cached
		processData: false,        // To send DOMDocument or non processed data file it is set to false

		dataType: 'json',
		success: function(res) {
			console.log('ok');
			console.log(res);
			var ttext = JSON.stringify(res.msg);
			$('#result_form_uch').addClass('dwizm');
			if(res.no != 'no') {
				$('#result_form_uch').html('<div class="result_form-wrapper"><p class="tittle-name_block">Модерация сохранилась!</p><button class="secondary" onclick="clos();">Закрыть</button></div>');
			} else {
				$('#result_form_uch').html('<div class="result_form-wrapper"><p class="tittle-name_block">' + ttext.replace(/\"/g, '') + '</p><button class="secondary" onclick="closModer();">Ок</button></div>');
			}
		},
		error: function(p1, p2, p3) {
			console.log(p1);
			console.log(p2);
			console.log(p3);
		}
	});

	return false;
}
</script>
