<?
$MESS ['AUTH_FORGOT_PASSWORD_2'] = "выбери, какую информацию использовать для изменения пароля:";
$MESS ['AUTH_GET_CHECK_STRING'] = "выслать контрольную строку";
$MESS ['AUTH_SEND'] = "выслать";
$MESS ['AUTH_AUTH'] = "авторизация";
$MESS["AUTH_LOGIN_EMAIL"] = "логин или email";
$MESS["system_auth_captcha"] = "введи символы с картинки";
$MESS["forgot_pass_email_note"] = "контрольная строка для смены пароля, а также твои регистрационные данные, будут высланы на email.";
$MESS["forgot_pass_phone_number"] = "номер телефона";
$MESS["forgot_pass_phone_number_note"] = "на ваш номер телефона будет выслано СМС с кодом для смены пароля.";
?>