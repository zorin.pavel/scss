<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH ."/css/login.css");
?>

    <div class="sub-main">
        <div class="background">
            <div class="box">

<?
    if(!empty($arParams["~AUTH_RESULT"])) {
        $text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
        ?><div class="alert <?=($arParams["~AUTH_RESULT"]["TYPE"] == "OK"? "alert-success":"alert-danger")?>"><?=nl2br(htmlspecialcharsbx($text))?></div><?
    }
?>

	<h3 class="bx-title" style='display:none;'><?=GetMessage("AUTH_GET_CHECK_STRING")?></h3>

	<p class="bx-authform-content-container" style='display:none;'><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></p>

	<form name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
        <?if($arResult["BACKURL"] <> ''):?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
        <?endif?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="SEND_PWD">

			<p class="tittle-name_block"><?echo GetMessage("AUTH_LOGIN_EMAIL")?></p>

            <input type="text" class="input-form input-form_first  border"  name="USER_LOGIN" maxlength="255" value="<?=$arResult["USER_LOGIN"]?>" />
            <input type="hidden" name="USER_EMAIL" />

			<div class="bx-authform-note-container"><?echo GetMessage("forgot_pass_email_note")?></div>

<?if($arResult["PHONE_REGISTRATION"]):?>
		<div class="bx-authform-formgroup-container">
			<div class="bx-authform-label-container"><?echo GetMessage("forgot_pass_phone_number")?></div>
			<div class="bx-authform-input-container">
				<input type="text" class="input-form input-form_first  border"  name="USER_PHONE_NUMBER" maxlength="255" value="<?=$arResult["USER_PHONE_NUMBER"]?>" />
			</div>
			<div class="bx-authform-note-container"><?echo GetMessage("forgot_pass_phone_number_note")?></div>
		</div>
<?endif?>

<?if ($arResult["USE_CAPTCHA"]):?>
		<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />

		<div class="bx-authform-formgroup-container">
			<div class="bx-authform-label-container"><?echo GetMessage("system_auth_captcha")?></div>
			<div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /></div>
			<div class="bx-authform-input-container">
				<input class="input-form input-form_first  border"  type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
			</div>
		</div>

<?endif?>

        <button type="submit" class="btn btn-primary" name="send_account_info"><?=GetMessage("AUTH_SEND")?></button>
        <a role="button" class="flat" href="<?=$arResult["AUTH_AUTH_URL"]?>"><?=GetMessage("AUTH_AUTH")?></a>

	</form>

            </div>
        </div>
    </div>


<script type="text/javascript">
document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;};
document.bform.USER_LOGIN.focus();
</script>
