<?
    define("NEED_AUTH", true);
    if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
        die();

    IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");

    $APPLICATION->SetTitle("");
	$APPLICATION->AddHeadString('<meta name="viewport" content="width=device-width, initial-scale=1" />');
    $urll = $_SERVER['REQUEST_URI'];
    $urll = explode('?', $url);
    $urll = $url[0];
    echo $url[0];

    if($_SERVER['HTTP_HOST']==='2021.xn--89-dlcaps4dk1f.xn--p1ai' && strlen($url[0])<2) {
        header("Location: https://2021.xn--89-dlcaps4dk1f.xn--p1ai/2021/");
    }

    $APPLICATION->ShowHead();
    use Bitrix\Main\Page\Asset;
//  Asset::getInstance()->addCss("/local/css/style.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH ."/css/style-v2.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH ."/css/jquery-ui.min.css");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-3.6.0.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-ui.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/main.js");


?>
<?$APPLICATION->ShowPanel();?>

    <div class="page-container">
            <header>
                <svg role="icon button" class="secondary hum-icon" onClick="toggleMenu(true);">
                    <use xlink:href='<?=SITE_TEMPLATE_PATH?>/icons/menu.svg#icon' href="<?=SITE_TEMPLATE_PATH?>/icons/menu.svg#icon"></use>
                </svg>
                <div class="menu" onClick="toggleMenu(false);">
                    <nav>
                        <svg role="icon" class="close-icon" onClick="toggleMenu(false);">
                            <use xlink:href='<?=SITE_TEMPLATE_PATH?>/icons/close.svg#icon' href="<?=SITE_TEMPLATE_PATH?>/icons/close.svg#icon"></use>
                        </svg>
                        <?
                            $APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "horizontal_multilevel1",
                                Array(
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "CHILD_MENU_TYPE" => "left",
                                    "DELAY" => "N",
                                    "MAX_LEVEL" => "2",
                                    "MENU_CACHE_GET_VARS" => array(""),
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_THEME" => "site",
                                    "ROOT_MENU_TYPE" => "top",
                                    "USE_EXT" => "N"
                                    )
                                );
                        ?>
                    </nav>
                    <div class="cover"></div>
                </div>
                <a role="button" target="_blank" href="https://xn---89-redfq.xn--p1ai/">На страницу премии</a>
            </header>
        <main>
<?
    function isContestTime() {
		$from = new DateTime();
		return $from > (new DateTime('2024-06-03 00:00:00')) && $from < (new DateTime('2024-06-14 00:00:00'));
	}
?>
