    </main>
    <footer>
        <div class="footer-line">
            <div class="footer-contact">
                <h2 class="footer-contact-title">Контакты технической поддержки</h2>
                <p class="footer-contact-paragraph"><a href="https://vk.com/im?media=&sel=-199712030" target="_blank" class="underlined">Напиши нам</a> ВКонтакте, и мы поможем!</p>
            </div>
            <a href="https://vk.com/top.yamal" target="_blank" class="footer-vk-button">
                <img width="57" alt="VK" src="/upload/medialibrary/5c6/u2gtj8r1r87q8jaw3r1ind08i6dv3zia.png" height="36" title="VK">
            </a>
        </div>
        <div class="footer-line">
            <div class="footer-partners-logos">
                <img width="160" alt="Я молод" src="/upload/medialibrary/1f3/r8wywzxqa5o898x06p9cq957wyvtowgv.png" height="41" title="Я молод">
                <img width="297" src="/upload/medialibrary/96b/0069mbq19khagrrlmf8xn120ycuy4nbr.png" height="29" title="Росмолодежь" alt="Росмолодежь">
                <img width="183" alt="Нацпроект образование" src="/upload/medialibrary/998/53z1m4ulqjfrtzx80qqxluf5oamot846.png" height="42" title="Нацпроект образование">
            </div>
        </div>
    </footer>
</div>

<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date(); for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }} k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(91549349, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script>  <!-- /Yandex.Metrika counter -->
